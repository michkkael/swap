/**
 * Created by mickaelchanrion on 07/06/2016.
 */

var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    name: String,
    slug: String,
    pushToken: String
});

var userModel = mongoose.model('user', userSchema);

module.exports = userModel;