/**
 * Created by mickaelchanrion on 07/06/2016.
 */

var mongoose = require('mongoose');

var newSchema = mongoose.Schema({
    type: String,
    icon: String,
    title: String,
    user: {
        slug: String,
        name: String,
        theme: Number,
        avatar: Number,
        humor: Number
    },
    comment: String,
    photo: String,
    date: {
        type:Date,
        default: Date.now
    }
});

var newModel = mongoose.model('new', newSchema);

module.exports = newModel;