/**
 * Created by mickaelchanrion on 19/05/2016.
 */

// Requires
var express = require('express'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    pushbots = require('pushbots'),
    userModel = require('./models/user.js'),
    newModel = require('./models/new.js');

// Create an Express App
var app = express();

app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit: 50000}));
app.use(bodyParser.json({limit: "50mb"}));


/*****************
 *
 *  HTTP SERVER
 *
 *****************/

var server = require('http').createServer(app);
var serverPort = process.env.PORT || 4666;

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});


/*****************
 *
 *  NOTIFICATION
 *
 *****************/

var Pushbots = new pushbots.api({
    id: '5755f5bf4a9efadcf48b4567',
    secret: '0d081ae19df42baf3d8f647e4825ee99'
});

function sendNotification(message, params, pushToken) {

    message = (typeof message === 'undefined') ? "Default message" : message;

    if (message) Pushbots.setMessage(message);
    if (params) Pushbots.customFields({"params": params});

    /** Send to a unique user **/
    if (pushToken) {
        Pushbots.pushOne(pushToken, function (response) {
            return response;
        });

    } else {
        Pushbots.push(function (response) {
            return response;
        });
    }
}


/*******************
 *
 *    API ROUTING
 *
 *******************/

// Enable CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// Router
var apiRouter = express.Router();

// Connect to database
//mongoose.connect('mongodb://localhost:27017/swap');
mongoose.connect('mongodb://swap:crm15Swap@ds025232.mlab.com:25232/swap');


/*******************
 *    Users
 *******************/

/** Get all users **/
//apiRouter.get('/users', function (req, res) {});

/** Get a user by name **/
//apiRouter.get('/user/:name', function (req, res) {});

/** Add a user **/
apiRouter.post('/users', function(req, res) {

    var user = new userModel();

    if (req.body.name) user.name = req.body.name;
    if (req.body.slug) user.slug = req.body.slug;
    if (req.body.pushToken) user.pushToken = req.body.pushToken;

    user.save(function (err) {

        if (!err) {
            res.status(201).json({message: "server:userAdded"})
        } else {
            res.status(500).json({message: "server:userNotAdded"});
            console.log(err)
        }
    });

});

/** Update a user **/
//apiRouter.put('/user', function (req, res) {});

/** Delete a user by name **/
//apiRouter.delete('/user/:name', function (req, res) {});


/*******************
 *    News
 *******************/

/** Get all news **/
apiRouter.get('/news', function (req, res) {

    newModel.find({}, null, {sort: '-date'}, function (err, news) {
        if (!err) {
            return res.send(news);
        } else {
            console.log(err);
        }
    });
});

/** Get a new by id **/
//apiRouter.get('/new/:id', function (req, res) {});

/** Add a new **/
apiRouter.post('/news', function(req, res) {

    var news = new newModel();

    if (req.body.type) news.type = req.body.type;
    if (req.body.icon) news.icon = req.body.icon;
    if (req.body.title) news.title = req.body.title;
    if (req.body.user) {
        var user = req.body.user;
        if (user.slug) news.user.slug = user.slug;
        if (user.name) news.user.name = user.name;
        if (user.theme) news.user.theme = user.theme;
        if (user.avatar) news.user.avatar = user.avatar;
        if (user.humor) news.user.humor = user.humor;
    }
    if (req.body.comment) news.comment = req.body.comment;
    if (req.body.photo) news.photo = req.body.photo;
    if (req.body.date) news.date = req.body.date;

    news.save(function (err, news) {

        if (!err) {
            res.status(201).json({message: "server:newsAdded"})
        } else {
            res.status(500).json({message: "server:newsNotAdded"});
            console.log(err)
        }
    });
});

/** Update a user **/
//apiRouter.put('/new', function (req, res) {});

/** Delete a user by name **/
//apiRouter.delete('/new/:id', function (req, res) {});


/*******************
 *    Notification
 *******************/

/** Send notification **/
apiRouter.post('/notify', function (req, res) {

    if (req.body.recipient.slug) {

        userModel.findOne({slug: req.body.recipient.slug}, function (err, user) {

            if (err) {
                console.log(err);
                res.send({message: err});

            } else if (user) {

                sendNotification(req.body.message, req.body.params, user.pushToken);
                res.send({message: "notification send"});

            } else {
                console.log("user not found");
                res.send({message: "user not found"});
            }

        });

    } else {

        sendNotification(req.body.message, req.body.params);
        res.send({message: "notification send"});
    }
});


// Routing
app.use('/api', apiRouter);

/*****************
 *
 *  START
 *
 *****************/

// Start server
server.listen(serverPort, function () {
    console.log('Server starts on port:' + serverPort);
});

/** Set true to add this news ! **/
if (false) {

    var data = [
        {
            "type": "task",
            "icon": "toast",
            "title": "Sylvie vient de préparer le petit déjeuner",
            "user": {
                "slug": "sylvie",
                "name": "Sylvie",
                "theme": 3,
                "avatar": 4,
                "humor": 1
            },
            "comment": "Bon appétit mes Wistiti !",
            "photo": null,
            "date": "2016-06-20T05:42:11.761Z"
        },
        {
            "type": "task",
            "icon": "wc",
            "title": "Maxime vient de nettoyer les toilettes",
            "user": {
                "slug": "maxime",
                "name": "Maxime",
                "theme": 1,
                "avatar": 1,
                "humor": 4
            },
            "comment": null,
            "photo": null,
            "date": "2016-06-20T14:42:11.761Z"
        },
        {
            "type": "hobby",
            "icon": "flower",
            "title": "Sylvie vient de faire du jardinage",
            "user": {
                "slug": "sylvie",
                "name": "Sylvie",
                "theme": 3,
                "avatar": 4,
                "humor": 2
            },
            "comment": "Adieu mon vernis !",
            "photo": null,
            "date": "2016-06-20T15:42:11.761Z"
        },
        {
            "type": "hobby",
            "icon": "jogging",
            "title": "Maxime vient de faire du jogging",
            "user": {
                "slug": "maxime",
                "name": "Maxime",
                "theme": 1,
                "avatar": 1,
                "humor": 1
            },
            "comment": "Et maintenant, je vais prendre une bonne douche !",
            "photo": null,
            "date": "2016-06-20T15:43:11.761Z"
        },
        {
            "type": "hobby",
            "icon": "video-game",
            "title": "Thierry vient de jouer à un jeu-vidéo",
            "user": {
                "slug": "thierry",
                "name": "Thierry",
                "theme": 4,
                "avatar": 3,
                "humor": 4
            },
            "comment": "C'est pas facile !",
            "photo": null,
            "date": "2016-06-20T16:42:11.761Z"
        },
        {
            "type": "task",
            "icon": "cook",
            "title": "Clémence vient de préparer à manger",
            "user": {
                "slug": "clemence",
                "name": "Clémence",
                "theme": 5,
                "avatar": 2,
                "humor": 2
            },
            "comment": null,
            "photo": null,
            "date": "2016-06-20T17:42:11.761Z"
        },
        {
            "type": "comment",
            "icon": null,
            "title": "Sylvie :",
            "user": {
                "slug": "sylvie",
                "name": "Sylvie",
                "theme": 3,
                "avatar": 4,
                "humor": 1
            },
            "comment": "Ça a l'air bon !",
            "photo": null,
            "date": "2016-06-20T17:43:11.761Z"
        },
        {
            "type": "task",
            "icon": "dishes",
            "title": "Thierry vient de mettre la table",
            "user": {
                "slug": "thierry",
                "name": "Thierry",
                "theme": 4,
                "avatar": 3,
                "humor": 2
            },
            "comment": null,
            "photo": null,
            "date": "2016-06-20T17:44:11.761Z"
        },
        {
            "type": "task",
            "icon": "dishes",
            "title": "Maxime vient de débarrasser la table",
            "user": {
                "slug": "maxime",
                "name": "Maxime",
                "theme": 1,
                "avatar": 1,
                "humor": 3
            },
            "comment": null,
            "photo": null,
            "date": "2016-06-20T18:44:11.761Z"
        },
        {
            "type": "task",
            "icon": "wash-dishes",
            "title": "Maxime vient de faire la vaisselle",
            "user": {
                "slug": "maxime",
                "name": "Maxime",
                "theme": 1,
                "avatar": 1,
                "humor": 3
            },
            "comment": null,
            "photo": null,
            "date": "2016-06-20T18:52:11.761Z"
        },
        {
            "type": "task",
            "icon": "washing-machine",
            "title": "Clémence vient de faire la lessive",
            "user": {
                "slug": "clemence",
                "name": "Clémence",
                "theme": 5,
                "avatar": 2,
                "humor": 1
            },
            "comment": "Un jeu d'enfant !",
            "photo": null,
            "date": "2016-06-20T18:52:15.761Z"
        },
        {
            "type": "hobby",
            "icon": "laptop",
            "title": "Maxime vient d'aller sur les réseaux sociaux",
            "user": {
                "slug": "maxime",
                "name": "Maxime",
                "theme": 1,
                "avatar": 1,
                "humor": 1
            },
            "comment": null,
            "photo": null,
            "date": "2016-06-20T19:00:11.761Z"
        },
        {
            "type": "hobby",
            "icon": "laptop",
            "title": "Clémence vient de surfer sur le Web",
            "user": {
                "slug": "clemence",
                "name": "Clémence",
                "theme": 5,
                "avatar": 2,
                "humor": 1
            },
            "comment": null,
            "photo": null,
            "date": "2016-06-20T19:01:11.761Z"
        },
        {
            "type": "hobby",
            "icon": "tv",
            "title": "Sylvie vient de regarder la chaîne sport",
            "user": {
                "slug": "sylvie",
                "name": "Sylvie",
                "theme": 3,
                "avatar": 4,
                "humor": 1
            },
            "comment": "Au moins, j'ai pu essayer un nouveau vernis !",
            "photo": null,
            "date": "2016-06-20T19:03:11.761Z"
        },

        {
            "type": "task",
            "icon": "toast",
            "title": "Maxime vient de préparer le petit déjeuner",
            "user": {
                "slug": "maxime",
                "name": "Maxime",
                "theme": 1,
                "avatar": 1,
                "humor": 2
            },
            "comment": null,
            "photo": null,
            "date": "2016-06-21T05:42:11.761Z"
        },
        {
            "type": "hobby",
            "icon": "laptop",
            "title": "Sylvie vient d'aller sur les réseaux sociaux",
            "user": {
                "slug": "sylvie",
                "name": "Sylvie",
                "theme": 3,
                "avatar": 4,
                "humor": 2
            },
            "comment": null,
            "photo": null,
            "date": "2016-06-21T19:00:11.761Z"
        },
        {
            "type": "task",
            "icon": "ironing",
            "title": "Thierry vient de faire du repassage",
            "user": {
                "slug": "thierry",
                "name": "Tierry",
                "theme": 4,
                "avatar": 3,
                "humor": 2
            },
            "comment": null,
            "photo": null,
            "date": "2016-06-21T19:10:11.761Z"
        },
        {
            "type": "task",
            "icon": "cook",
            "title": "Thierry vient de préparer à manger",
            "user": {
                "slug": "thierry",
                "name": "Thierry",
                "theme": 4,
                "avatar": 3,
                "humor": 4
            },
            "comment": "Désolé, ça va être salé...",
            "photo": null,
            "date": "2016-06-21T19:20:11.761Z"
        },
        {
            "type": "task",
            "icon": "wc",
            "title": "Maxime vient de nettoyer les toilettes",
            "user": {
                "slug": "maxime",
                "name": "Maxime",
                "theme": 1,
                "avatar": 1,
                "humor": 4
            },
            "comment": null,
            "photo": null,
            "date": "2016-06-21T19:22:11.761Z"
        },
        {
            "type": "task",
            "icon": "dishes",
            "title": "Sylvie vient de mettre la table",
            "user": {
                "slug": "sylvie",
                "name": "Sylvie",
                "theme": 3,
                "avatar": 4,
                "humor": 2
            },
            "comment": null,
            "photo": null,
            "date": "2016-06-21T19:23:11.761Z"
        },
        {
            "type": "task",
            "icon": "dishes",
            "title": "Clémence vient de débarrasser la table",
            "user": {
                "slug": "clemence",
                "name": "Clémence",
                "theme": 5,
                "avatar": 2,
                "humor": 2
            },
            "comment": null,
            "photo": null,
            "date": "2016-06-21T19:24:11.761Z"
        },
        {
            "type": "task",
            "icon": "dustbin",
            "title": "Sylvie vient de sortir les poubelles",
            "user": {
                "slug": "sylvie",
                "name": "Sylvie",
                "theme": 3,
                "avatar": 4,
                "humor": 5
            },
            "comment": "Bonjour l'odeur...",
            "photo": null,
            "date": "2016-06-21T19:25:11.761Z"
        },
        {
            "type": "task",
            "icon": "wash-dishes",
            "title": "Sylvie vient de faire la vaisselle",
            "user": {
                "slug": "sylvie",
                "name": "Sylvie",
                "theme": 3,
                "avatar": 4,
                "humor": 4
            },
            "comment": "Encore adieu à mon vernis !",
            "photo": null,
            "date": "2016-06-21T19:26:11.761Z"
        },
        {
            "type": "hobby",
            "icon": "tv",
            "title": "Maxime vient de regarder un documentaire",
            "user": {
                "slug": "maxime",
                "name": "Maxime",
                "theme": 1,
                "avatar": 1,
                "humor": 2
            },
            "comment": "Très intéressant ce documentaire sur les abeilles",
            "photo": null,
            "date": "2016-06-21T19:27:11.761Z"
        },
        {
            "type": "hobby",
            "icon": "book",
            "title": "Clémence vient de lire un livre",
            "user": {
                "slug": "clemence",
                "name": "Clémence",
                "theme": 5,
                "avatar": 2,
                "humor": 2
            },
            "comment": null,
            "photo": null,
            "date": "2016-06-21T19:28:11.761Z"
        },
        {
            "type": "hobby",
            "icon": "music",
            "title": "Sylvie vient d'écouter du Pop-rock",
            "user": {
                "slug": "sylvie",
                "name": "Sylvie",
                "theme": 3,
                "avatar": 4,
                "humor": 2
            },
            "comment": "Je ne pensais pas aimer !",
            "photo": null,
            "date": "2016-06-21T19:29:11.761Z"
        }
    ];

    for (var i = 0; i < data.length; i++) {
        var req = {};
        req.body = data[i];

        var news = new newModel();

        if (req.body.type) news.type = req.body.type;
        if (req.body.icon) news.icon = req.body.icon;
        if (req.body.title) news.title = req.body.title;
        if (req.body.user) {
            var user = req.body.user;
            if (user.slug) news.user.slug = user.slug;
            if (user.name) news.user.name = user.name;
            if (user.theme) news.user.theme = user.theme;
            if (user.avatar) news.user.avatar = user.avatar;
            if (user.humor) news.user.humor = user.humor;
        }
        if (req.body.comment) news.comment = req.body.comment;
        if (req.body.photo) news.photo = req.body.photo;
        if (req.body.date) news.date = req.body.date;

        news.save(function (err) {

            console.log(err);
        });
    }
}