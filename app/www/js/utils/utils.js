/**
 * Created by mickaelchanrion on 31/05/2016.
 */

function Utils() {
}

/**
 * Get a random int in a range
 * @param min Minimum value
 * @param max Maximum value
 * @returns {int}
 */
Utils.getRandomInt = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

/**
 * Highlight a text
 * @param $el a jQuery element
 */
Utils.highlightText = function ($el) {
    $el.html(
        '<span class="highlight-text-' + Utils.getRandomInt(1, 4) + '">' +
        $el.html() + '<svg class="icon"><use xlink:href="images/svg/sprite.symbol.svg#highlight"></use></svg></span>');
};

/**
 * Create a separator
 * @returns {string}
 */
Utils.getSeparator = function () {
    return '<span class="separator-' + Utils.getRandomInt(1, 2) + '">' +
        '<svg class="icon"><use xlink:href="images/svg/sprite.symbol.svg#line"></use></svg></span>';
};

/**
 * Create a heading title
 * @param title
 * @returns {string}
 */
Utils.getHeadingTitle = function (title) {
    return '<div class="heading-' + Utils.getRandomInt(1, 2) + '">' +
        '<span>' + title + '</span>' +
        '<svg class="icon"><use xlink:href="images/svg/sprite.symbol.svg#line"></use></svg>' +
        '</div>';
};

/**
 * Post data
 * @param url
 * @param data
 * @param successCb
 * @param errorCb
 */
Utils.post = function (url, data, successCb, errorCb) {

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        crossDomain: true,
        success: (function (successCb) {
            successCb && successCb();
        }).bind(this, successCb),
        error: (function () {
            console.log("request failed");
            errorCb && errorCb();
        }).bind(this, errorCb)
    });
};

/**
 * Get the current color theme
 * @param format hex || rgb (default)
 * @returns {*}
 */
Utils.getThemeColor = function (format) {

    var rgb = document.defaultView.getComputedStyle(document.getElementById("main-header"), null).getPropertyValue("background-color")

    if (format && format == "hex") {
        rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        return Utils.rgbToHex(rgb[1], rgb[2], rgb[3]);
    } else return rgb;
};

/**
 * Shade color (to get a color lighter/darker)
 * @param color Color hex code
 * @param percent Can be negative (i.e: -0.5 === -50%)
 * @returns {string} New color hex code
 */
Utils.shadeColor = function (color, percent) {

    var f = parseInt(color.slice(1), 16),
        t = percent < 0 ? 0 : 255,
        p = percent < 0 ? percent * -1 : percent,
        R = f >> 16, G = f >> 8 & 0x00FF, B = f & 0x0000FF;

    return "#" + (0x1000000 + (Math.round((t - R) * p) + R) * 0x10000 + (Math.round((t - G) * p) + G) * 0x100 + (Math.round((t - B) * p) + B)).toString(16).slice(1);
};

/**
 * Convert rgb color to color hex code
 * @param r Red value
 * @param g Green value
 * @param b Blue value
 * @returns {string}
 */
Utils.rgbToHex = function (r, g, b) {
    return "#" + (function(h){return new Array(7-h.length).join("0")+h})((r << 16 | g << 8 | b).toString(16).toUpperCase());
};

/**
 * Convert color hex code to rgb color
 * @param hex Color hex code
 * @returns {*}
 */
Utils.hexToRgb = function (hex) {

    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
};
