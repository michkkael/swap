/**
 * Created by mickaelchanrion on 24/05/2016.
 */

/** Config require **/
requirejs.config({
    baseUrl: 'js',
    paths: {
        jquery: '../node_modules/jquery/dist/jquery.min',
        underscore: '../node_modules/underscore/underscore-min',
        backbone: '../node_modules/backbone/backbone-min',
        tweenmax: '../node_modules/gsap/src/minified/TweenMax.min',
        moment: '../node_modules/moment/min/moment-with-locales.min',
        utils: './utils/utils',
        router: './app/router/router',
        collections: './app/collections',
        models: './app/models',
        templates: './app/templates',
        views: './app/views',
        events: './app/events',
        offCanvasNavigation: './lib/offCanvasNavigation',
        draggable: './lib/Draggable',
        throwProps: './lib/ThrowProps',
        roundProps: './lib/RoundProps',
        customEase: './lib/CustomEase',
        hammer: './lib/hammer'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        tweenmax: {
            deps: ['roundProps', 'customEase'],
            exports: 'TweenMax'
        },
        hammer: {
            deps: ['jquery'],
            exports: 'Hammer'
        },
        draggable: {
            deps: ['tweenmax']
        },
        throwProps: {
            deps: ['tweenmax']
        },
        offCanvasNavigation: {
            deps: ['tweenmax', 'draggable', 'throwProps']
        },
        utils: {
            exports: 'Utils'
        }
    }
});

/** Starts require **/
requirejs(['jquery', 'app/views/appView', 'moment', 'hammer'], function ($, App, Moment, Hammer) {

    Moment.locale('fr');

    /** Launch application **/
    if (window.cordova) document.addEventListener("deviceReady", deviceReady, false);
    else  {
        $(document).hammer();
        new App();
    }

    function deviceReady () {
        $(document).hammer();
        new App();
        document.removeEventListener("deviceReady", deviceReady, false);
    }
});