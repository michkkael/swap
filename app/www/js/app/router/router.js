/**
 * Created by mickaelchanrion on 19/05/2016.
 */

define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {

    var Router = Backbone.Router.extend({

        /** Routes **/
        routes: {
            '': 'home',
            'my-swap': 'mySwap'
        },

        /** Start **/
        start: function () {
            /** Start listening URLs **/
            Backbone.history.start();
        },

        /** Routes **/
        home: function () {
            this.trigger('home');
        },

        mySwap: function () {
            this.trigger('my-swap');
        }
    });

    return Router;
});
