/**
 * Created by mickaelchanrion on 24/05/2016.
 */

define(
    ['backbone'],
    function (Backbone) {

        return Backbone.Model.extend({

            defaults: {
                type: 'task',
                icon: 'toast',
                title: 'title',
                points: 50,
                doBefore: null,
                achieved: false
            }
        });
    }
);

