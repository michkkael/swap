/**
 * Created by mickaelchanrion on 02/06/2016.
 */

define(
    ['backbone'],
    function (Backbone) {

        return Backbone.Model.extend({

            defaults: {
                title: "a dialog view"
            }
        });
    }
);