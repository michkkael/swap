/**
 * Created by mickaelchanrion on 02/06/2016.
 */

define(
    ['backbone'],
    function (Backbone) {

        return Backbone.Model.extend({

            url: 'api/profile.json',
            defaults: {
                user: {
                    slug: "user-slug"
                },
                score: 0,
                promise: null,
                successes: null
            }
        });
    }
);