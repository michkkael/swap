/**
 * Created by mickaelchanrion on 24/05/2016.
 */

define(
    ['backbone', 'models/todoModel'],
    function (Backbone, TodoModel) {

        return Backbone.Collection.extend({

            //url: 'http://swap-16.herokuapp.com/api/test',
            url: 'api/todos.json',
            model: TodoModel
        });
    }
);