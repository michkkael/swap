/**
 * Created by mickaelchanrion on 24/05/2016.
 */

define(
    ['backbone', 'models/newModel'],
    function (Backbone, NewModel) {

        return Backbone.Collection.extend({

            //url: 'api/news.json',
            //url: 'http://localhost:4666/api/news',
            url: 'http://swap-16.herokuapp.com/api/news',
            model: NewModel
        });
    }
);