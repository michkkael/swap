/**
 * Created by mickaelchanrion on 19/05/2016.
 */

define(
    ['jquery', 'underscore', 'backbone', 'router', 'utils', 'events/eventBus', 'views/loaderView', 'views/headerView', 'views/homeView', 'views/dialogView'],
    function ($, _, Backbone, Router, Utils, EventBus, LoaderView, HeaderView, HomeView, DialogView) {

        return Backbone.View.extend({

            /** Html **/
            el: 'body',

            /** Constructor **/
            initialize: function () {

                if (!this.PushBots) this.initPushBots();

                if (!this.loaderView) {

                    this.setColorStatusBar();

                    /** Initialize loader **/
                    this.loaderView = new LoaderView();
                    this.loaderHidden = false;

                    /** Add theme class on body **/
                    this.$el.attr("data-theme", window.localStorage.getItem("userTheme"));

                    if (!this.loaderView.isReady) {

                        /** Waiting loaderView, when it's done, continue initialization **/
                        this.listenToOnce(this.loaderView, "loaderView:ready", this.initialize);

                        return;
                    }
                }

                EventBus.on("Camera:open", this.openCamera, this);

                this.currentView = null;

                this.container = this.$('#main');
                
                this.header = new HeaderView({el: '#main-header'});

                this.dialogView = new DialogView();

                this.router = new Router();
                this.listenTo(this.router, "home", this.goHome);
                this.listenTo(this.router, "my-swap", this.showMySwap);
                this.router.start();

                /** If user is not registered **/
                if (!window.localStorage.getItem('userRegistered')) {
                    this.registerUser();
                    return;
                }
            },

            /** Push notifications **/
            initPushBots: function () {

                if (!window.plugins) return;

                console.log("initPushBots");

                this.PushBots = window.plugins.PushbotsPlugin;

                this.PushBots.initialize("5755f5bf4a9efadcf48b4567", {"android": {"sender_id": "1051578139743"}});

                this.PushBots.getRegistrationId((function (token) {
                    this.pushToken = token;
                    console.log(token);
                }).bind(this));

                //this.PushBots.on("notification:received", this.notificationReceived.bind(this));

                this.PushBots.on("notification:clicked", this.notificationClicked.bind(this));
            },

            notificationReceived: function (data) {
                /** Do nothing **/
            },

            notificationClicked: function (data) {

                var params = JSON.parse(data.params);

                switch (params.subject) {

                    case "request":
                        EventBus.trigger("Dialog:open", {
                            template: "answerRequest",
                            subject: "answerRequest",
                            title: params.sender.name + " a besoin d'aide !",
                            message: params.sender.name + " demande ton aide pour <br> " + params.todo.title.toLowerCase(),
                            recipient: {
                                slug: params.sender.slug,
                                name: params.sender.name
                            },
                            sender: {
                                slug: window.localStorage.getItem('userSlug'),
                                name: window.localStorage.getItem('userName')
                            },
                            todo: {
                                type: params.todo.type,
                                title: params.todo.title,
                                icon: params.todo.icon
                            },
                            swapUser: {
                                avatar: window.localStorage.getItem("swapUserAvatar"),
                                theme: window.localStorage.getItem("swapUserTheme")
                            }
                        });

                        break;

                    default: /** Do nothing **/
                }
            },

            /** Camera **/
            openCamera: function (params) {

                /**
                 * To open the camera: use this
                 * var EventBus = require('events/eventBus');
                 * EventBus.trigger("Camera:open", {args});
                 **/
                try {
                    navigator.camera.getPicture(
                        this.cameraSuccess.bind(this, params.successCallback),
                        this.cameraFail.bind(this, params.failCallback), {
                        quality: params.quality || 25,
                        destinationType: params.destinationType || Camera.DestinationType.DATA_URL,
                        targetWidth: params.targetWidth || 900,
                        targetHeight: params.targetHeight || 900,
                        cameraDirection: Camera.Direction.FRONT
                    });
                } catch (err) {
                    console.error("Impossible to open camera\n", err);
                }
            },

            cameraSuccess: function (cb, imageData) {

                cb && cb(imageData);
            },

            cameraFail: function (cb, err) {

                console.error(err);
                cb && cb(err);
            },

            /** Status bar with theme color **/
            setColorStatusBar: function () {

                try {
                    switch (window.localStorage.getItem('userTheme')) {
                        case "1":
                            StatusBar.backgroundColorByHexString("#9fcc7a");
                            break;
                        case "2":
                            StatusBar.backgroundColorByHexString("#a563c9");
                            break;
                        case "3":
                            StatusBar.backgroundColorByHexString("#c9b712");
                            break;
                        case "4":
                            StatusBar.backgroundColorByHexString("#c7963c");
                            break;
                        case "5":
                            StatusBar.backgroundColorByHexString("#32c292");
                            break;
                        case "6":
                            StatusBar.backgroundColorByHexString("#cc4580");
                            break;
                        default:
                            StatusBar.backgroundColorByHexString("#9fcc7a");
                    }
                } catch (err) {
                    console.error(err);
                }
            },

            /** User registration **/
            registerUser: function () {

                var storage = window.localStorage;

                /** User infos **/
                if (!storage.getItem('userName')) storage.setItem('userName', prompt("Nom de l'utilisateur") || "User");
                if (!storage.getItem('userSlug')) storage.setItem('userSlug', prompt("Slug de l'utilisateur") || "user");
                if (!storage.getItem('userTheme')) storage.setItem('userTheme', prompt("Theme de l'utilisateur") || "default");
                if (!storage.getItem('userAvatar')) storage.setItem('userAvatar', prompt("Avatar de l'utilisateur") || "1");
                if (!storage.getItem('userScore')) storage.setItem('userScore', prompt("Score de l'utilisateur") || "100");

                /** Swap infos **/
                if (!storage.getItem('swapUserName')) storage.setItem('swapUserName', prompt("Nom de l'utilisateur Swappé") || "User Swap");
                if (!storage.getItem('swapUserSlug')) storage.setItem('swapUserSlug', prompt("Slug de l'utilisateur Swappé") || "swapUser-swap");
                if (!storage.getItem('swapUserTheme')) storage.setItem('swapUserTheme', prompt("Theme de l'utilisateur Swappé") || "2");
                if (!storage.getItem('swapUserAvatar')) storage.setItem('swapUserAvatar', prompt("Avatar de l'utilisateur Swappé") || "2");
                if (!storage.getItem('swapUserScore')) storage.setItem('swapUserScore', prompt("Score de l'utilisateur Swappé") || "100");

                var data = {
                    name: storage.getItem('userName'),
                    slug: storage.getItem('userSlug'),
                    pushToken: this.pushToken
                };

                console.log(data);

                storage.setItem('userRegistered', "true");

                /** Store user in DB **/
                Utils.post(
                    "http://swap-16.herokuapp.com/api/users",
                    data,
                    function () {
                        window.localStorage.setItem('userInDB', "true");
                        alert("User successfully registered!\n Please restart app");
                    }, function () {
                        alert("Oouups! Something went wrong...\n User is not registered")
                    }
                );
            },

            /** View Management **/
            killView: function () {

                /** Remove current view **/
                if (this.currentView) {
                    
                    this.stopListening(this.currentView);
                    this.currentView.destroy();
                }
            },

            addView: function () {

                /** Add new view **/
                this.container.prepend(this.currentView.el);

                //this.currentView.added();

                /** If currentView is not ready, then listenTo, else hide loader **/
                if (!this.currentView.isReady) {
                    this.listenToOnce(this.currentView, 'view:ready', this.viewReady);
                } else {
                    _.defer(this.viewReady.bind(this))
                }
            },

            viewReady: function () {
                this.hideLoader();
            },

            /** Show / hide loader **/
            hideLoader: function () {

                /** Page ready, hide loader **/
                this.loaderHidden = true;
                this.loaderView.hide();
            },

            showLoader: function (method, params) {

                /** Show loader and return to previous method when it's done **/
                this.listenToOnce(this.loaderView, "loaderView:visible", method.bind(this, params));
                this.loaderView.show();
                this.loaderHidden = false;
            },

            /** Routes **/
            goHome: function () {

                /** If loader is hidden, then show it, else kill current view **/
                if (this.loaderHidden) {
                    this.showLoader(this.goHome);
                    return;
                } else {
                    this.killView();
                }

                /** Update current view **/
                this.currentView = new HomeView();

                /** Add view **/
                this.addView();
            },

            showMySwap: function () {

                /** Show loader **/
                this.loaderView.show((function () {

                    /** Listen tap to hide loader **/
                    this.loaderView.$el.one("tap", (function() {
                        this.loaderView.hide();
                        Backbone.history.navigate('#');
                    }).bind(this));

                }).bind(this));
            }
        });
    });
