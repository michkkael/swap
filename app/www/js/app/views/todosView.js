/**
 * Created by mickaelchanrion on 24/05/2016.
 */

define(
    ['jquery', 'underscore', 'backbone', 'tweenmax', 'collections/todosCollection', 'templates/todosTemplate', 'models/todoModel', 'views/todoView'],
    function ($, _, Backbone, TweenMax, TodosCollection, TodosTemplate, TodoModel, TodoView) {

        return Backbone.View.extend({

            icon: 'todo',
            title: "Dans la peau de "+ window.localStorage.getItem("swapUserName"),
            collection: new TodosCollection(),
            template: TodosTemplate,
            tagName: 'div',
            className: 'todos',

            initialize: function (params) {

                if (params) {
                    this.isChildView = _.has(params, "isChildView") ? params.isChildView : false;

                    if (this.isChildView)
                        this.order = params.order || null;
                }

                /** Ready **/
                this.isReady = false;

                this.childViews = {};

                this.collection.fetch().done(this.fetchDone.bind(this));
            },

            fetchDone: function () {
                this.render();
            },

            /** Show / Hide **/
            show: function () {
                TweenMax.to(this.$el, 0.8, {css: {opacity: 1}, ease: "easeOutQuint"});
            },

            hide: function () {

                TweenMax.to(this.$el, 0.8, {
                    css: {opacity: 0},
                    ease: "easeInQuint",
                    callbackScope: this,
                    onComplete: function() {
                        if (!this.isChildView)
                            this.trigger("view:hidden");
                        else
                            this.trigger("childView:hidden");
                    }
                });
            },

            /** Ready **/
            ready: function () {

                this.isReady = true;

                if (!this.isChildView)
                    this.trigger("view:ready");
                else
                    this.trigger("childView:ready");
            },

            added: function () {},

            /** Render **/
            render: function () {

                var todosNb = _.countBy(this.collection.models, function (model) {
                    if (model.attributes.achieved == false) return model.attributes.type;
                });

                if(!todosNb.task) todosNb.task = 0;
                if(!todosNb.hobby) todosNb.hobby = 0;

                this.$el.html(this.template(todosNb));

                $tasks = this.$el.find("#tasks");
                $hobbies = this.$el.find("#hobbies");

                var i = 0;
                _.each(this.collection.models, function (model) {

                    if (model.attributes.achieved == false) {

                        if (model.attributes.type == "task") {

                            this.childViews[i] = new TodoView({model: model});
                            this.childViews[i].$el.appendTo($tasks);
                            i++;

                        } else if (model.attributes.type == "hobby") {

                            this.childViews[i] = new TodoView({model: model});
                            this.childViews[i].$el.appendTo($hobbies);
                            i++;
                        }
                    }

                }, this);

                _.defer(this.ready.bind(this));

                return this;
            },

            /** Destruction **/
            destroy: function () {

                _.each(this.childViews, function (view) {
                    view.destroy();
                }, this);

                /** Events **/
                this.undelegateEvents();
                this.$el.removeData().unbind();

                /** Remove from DOM **/
                this.remove();
                Backbone.View.prototype.remove.call(this);
            }
        });
    });