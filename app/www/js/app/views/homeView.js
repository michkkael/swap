/**
 * Created by mickaelchanrion on 19/05/2016.
 */

define(
    ['jquery', 'underscore', 'backbone', 'tweenmax', 'offCanvasNavigation', 'events/eventBus', 'views/profileView', 'views/newsView', 'views/todosView'],
    function ($, _, Backbone, TweenMax, offCanvasNavigation, EventBus, ProfileView, NewsView, TodosView) {

        return Backbone.View.extend({

            tagName: 'section',
            className: 'home ocn',

            initialize: function () {

                /** Ready **/
                this.isReady = false;

                this.childViews = {
                    profileView: new ProfileView({isChildView: true, order: 1}),
                    todosView: new TodosView({isChildView: true, order: 2}),
                    newsView: new NewsView({isChildView: true, order: 3})
                };

                /** Listen all childViews **/
                _.each(this.childViews, function (view) {
                    this.listenToOnce(view, 'childView:ready', this.childViewReady.bind(this, view));
                }, this);

                /** Create ocn markup **/
                this.ocnMarkup = {
                    wrapper: $('<div class="ocn__wrapper"></div>'),
                    controls: $('<div class="ocn__controls"></div>')
                };

                var i = 1;
                _.each(this.childViews, function (view) {

                    this.ocnMarkup.wrapper
                        .append('<div class="ocn__slide" data-slide="' + i + '"></div>');

                    this.ocnMarkup.controls
                        .append('<div class="ocn__trigger" data-trigger="' + i + '">' +
                                    '<svg class="icon"><use xlink:href="images/svg/sprite.symbol.svg#'+ view.icon +'"></use></svg>' +
                                '</div>');

                    i++;
                }, this);

                this.ocnMarkup.wrapper.appendTo(this.$el);
                this.ocnMarkup.controls.appendTo(this.$el);
            },

            /** Show / Hide **/
            show: function () {
                TweenMax.to(this.$el, 0.8, {css: {autoAlpha: 1}, ease: "easeOutQuint"});
            },

            hide: function () {

                TweenMax.to(this.$el, 0.8, {
                    css: {autoAlpha: 0},
                    ease: "easeInQuint",
                    callbackScope: this,
                    onComplete: function() {
                        this.trigger("view:hidden");
                    }
                });
            },

            /** Ready **/
            ready: function () {

                this.isReady = true;
                this.trigger('view:ready');
            },

            childViewReady: function (view) {

                /** Add view to ocn slide **/
                view.$el.appendTo(this.ocnMarkup.wrapper.find('[data-slide="' + view.order + '"]'));

                /** Waiting for all views ready **/
                if (_.findWhere(this.childViews, {isReady: false}))
                    return;
                else
                    this.render();
            },

            added: function () {
            },

            /** Render **/
            render: function () {

                this.$ocnMarker = $("<div class='ocn__marker'></div>");

                /** Init ocn **/
                this.ocn = this.$el.offCanvasNavigation({
                    startSlide: 2,
                    onReady: this.onOcnReady.bind(this),
                    onSlideChange: this.onOcnSlideChange.bind(this)
                });

                _.defer((function() {
                        _.delay(this.ready.bind(this), 1000)
                }).bind(this));
            },

            /** On ocn ready, create marker and get positionX of each trigger **/
            onOcnReady: function () {

                var $controls = this.$el.find(".ocn__controls"),
                    positionX;

                this.ocnTriggersPositionX = {};

                var i = 1;
                _.each($controls.find(".ocn__trigger"), function (trigger) {
                    positionX = trigger.getBoundingClientRect();
                    positionX = positionX.left + (positionX.right - positionX.left) / 4 + 1;

                    this.ocnTriggersPositionX[i] = positionX;
                    i++;
                }, this);

                this.$ocnMarker.appendTo($controls);
            },

            /** On ocn slide changes, move marker **/
            onOcnSlideChange: function (oldIndex, currentIndex) {

                switch (currentIndex) {
                    case 1:
                        EventBus.trigger("Header:setTitle", this.childViews.profileView.title);
                        this.childViews.profileView.animateScore();
                        break;
                    case 2:
                        EventBus.trigger("Header:setTitle", this.childViews.todosView.title);
                        break;
                    case 3:
                        EventBus.trigger("Header:setTitle", this.childViews.newsView.title);
                        break;
                    default:
                        EventBus.trigger("Header:setTitle", "Swap");
                }

                setTimeout((function () {
                    TweenMax.to(this.$ocnMarker, .5, {
                        x: this.ocnTriggersPositionX[currentIndex],
                        ease: Power4.easeOut,
                        force3D: true
                    });
                }).bind(this), 10);
            },

            /** Destruction **/
            destroy: function () {

                _.each(this.childViews, function (view) {
                    view.destroy();
                }, this);

                /** Events **/
                this.undelegateEvents();
                this.$el.removeData().unbind();

                /** Remove DOM **/
                this.remove();
                Backbone.View.prototype.remove.call(this);
            }
        });
    });
