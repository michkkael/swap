/**
 * Created by mickaelchanrion on 24/05/2016.
 */

define(
    ['jquery', 'underscore', 'backbone', 'tweenmax', 'moment', 'utils', 'events/eventBus', 'collections/newsCollection', 'templates/newsTemplate', 'models/newModel', 'views/newView'],
    function ($, _, Backbone, TweenMax, Moment, Utils, EventBus, NewsCollection, NewsTemplate, NewModel, NewView) {

        return Backbone.View.extend({

            icon: 'news',
            title: "Fil d'actualité",
            collection: new NewsCollection(),
            template: NewsTemplate,
            tagName: 'div',
            className: 'news',

            events: {
                'tap .do-addNews': 'addNews'
            },

            initialize: function (params) {

                if (params) {
                    this.isChildView = _.has(params, "isChildView") ? params.isChildView : false;

                    if (this.isChildView) this.order = params.order || null;
                }

                /** Ready **/
                this.isReady = false;

                this.childViews = {};

                this.collection.fetch().done(this.fetchDone.bind(this));

                /** Listen add news request **/
                EventBus.on("News:add", this.add, this);
            },

            fetchDone: function () {

                this.render();
            },

            /** Show / Hide **/
            show: function () {

                var tl = new TimelineMax();
                tl.to(this.$el, 1, {
                    autoAlpha: 1,
                    ease: Quart.easeOut
                }).from(this.$wrapper, 1, {
                    y: 50,
                    ease: Quart.easeOut,
                    force3D: true
                }, 0);
            },

            hide: function (cb) {

                var tl = new TimelineMax();
                tl.to(this.$el,.8, {
                    autoAlpha: 0,
                    ease: Quart.easeIn,
                    callbackScope: this,
                    onComplete: function() {
                        if (!this.isChildView)
                            this.trigger("view:hidden");
                        else
                            this.trigger("childView:hidden");

                        cb && cb();
                    }
                }).to(this.$wrapper,.8, {
                    y: 50,
                    ease: Quart.easeIn,
                    force3D: true
                }, 0);
            },

            /** Ready **/
            ready: function () {

                this.isReady = true;

                if (!this.isChildView)
                    this.trigger("view:ready");
                else
                    this.trigger("childView:ready");

                if (this.isReloading) {
                    this.show();
                    this.isReloading = false;
                }
            },

            added: function () {},

            /** Render **/
            render: function () {

                /** If there's news **/
                if (this.collection.models.length == 0) {
                    this.$el.html('<div class="news__wrapper">' + Utils.getHeadingTitle("Pas d'actualités pour le moment") + '</div>');
                    _.defer(this.ready.bind(this));
                    return this;
                }

                /** Set template and get wrapper **/
                this.$el.html(this.template());
                this.$wrapper = this.$el.find(".news__wrapper");

                var today = new Date(),
                    $newsYesterday, $newsThisWeek,
                    newsNb = {
                        today: 0,
                        yesterday: 0,
                        thisWeek: 0
                    };

                /** Append views in right sections (today, yesterday or this week) **/
                var i = 0;

                _.each(this.collection.models, function (model) {

                    switch (Moment(today).diff(Moment(model.get("date")).format("YYYY-MM-DD"), 'days')) {
                        case 0:
                            if (newsNb.today == 0) {

                                this.$wrapper.append(Utils.getHeadingTitle("Aujourd'hui"));
                                this.$newsToday = $('<div class="news__list" id="news-today"></div>');
                                this.$newsToday.appendTo(this.$wrapper);

                            } else this.$newsToday.append(Utils.getSeparator());

                            this.childViews[i] = new NewView({model: model});
                            this.childViews[i].$el.appendTo(this.$newsToday);

                            newsNb.today++; i++;
                            break;

                        case 1:
                            if (newsNb.yesterday == 0) {

                                this.$wrapper.append(Utils.getHeadingTitle("Hier"));
                                $newsYesterday = $('<div class="news__list" id="news-yesterday"></div>');
                                $newsYesterday.appendTo(this.$wrapper);

                            } else $newsYesterday.append(Utils.getSeparator());

                            this.childViews[i] = new NewView({model: model});
                            this.childViews[i].$el.appendTo($newsYesterday);

                            newsNb.yesterday++; i++;
                            break;

                        default:
                            if (newsNb.thisWeek == 0) {

                                this.$wrapper.append(Utils.getHeadingTitle("Plus tôt dans la semaine"));
                                $newsThisWeek = $('<div class="news__list" id="news-this-week"></div>');
                                $newsThisWeek.appendTo(this.$wrapper);

                            } else $newsThisWeek.append(Utils.getSeparator());

                            this.childViews[i] = new NewView({model: model});
                            this.childViews[i].$el.appendTo($newsThisWeek);

                            newsNb.thisWeek++; i++;
                            break;
                    }
                }, this);

                this.createLoadTrigger();

                _.defer(this.ready.bind(this));

                return this;
            },

            createLoadTrigger: function () {

                var scope = this;

                this.loadTrigger = {};
                this.loadTrigger.$el = this.$el.find("#news__load-trigger");

                this.loadTrigger.draggable = Draggable.create(this.loadTrigger.$el[0], {
                    type: "y",
                    edgeResistance:.8,
                    zIndexBoost: false,
                    bounds: {
                        top: -document.getElementById("main-header").clientHeight*2,
                        left: 0,
                        width: window.innerWidth,
                        height: window.innerHeight/3
                    },
                    throwProps: true,
                    maxDuration:.3,
                    force3D: true,
                    dragResistance: 0.5,
                    snap: {
                        y: function () {
                            return 0;
                        }
                    },
                    onDragEnd: function () {
                        if (this.y >= this.maxY) {
                            scope.reload();
                        }
                    }
                });
            },

            reload: function () {

                this.isReloading = true;
                this.isReady = false;

                /** Hide collection, then fetch **/
                this.hide((function () {

                    this.collection.fetch().done((function () {

                        /** Re-render collection **/
                        this.render();

                    }).bind(this));
                }).bind(this));
            },

            /** On tap "add" button, show Dialog to add a news **/
            addNews: function () {

                EventBus.trigger("Dialog:open", {
                    template: "addNews",
                    title: "Quoi de neuf ?",
                    type: "news"
                })
            },

            /** Add a news to collection **/
            add: function (news) {

                /** Add data to collection **/
                this.collection.add(news, {at: 0});

                /** Create a new NewView as a childView **/
                var childViewsNb = _.size(this.childViews);
                this.childViews[childViewsNb] = new NewView({model: this.collection.models[0]});

                /** If there's no news today, then create the section **/
                if (!this.$newsToday) {

                    this.$newsToday = $('<div class="news__list" id="news-today"></div>');
                    this.$wrapper.prepend(this.$newsToday);
                    this.$wrapper.prepend(Utils.getHeadingTitle("Aujourd'hui"));

                    /** Add childView **/
                    this.childViews[childViewsNb].$el.prependTo(this.$newsToday);

                } else {

                    /** Add childView **/
                    this.$newsToday.prepend(Utils.getSeparator());
                    this.childViews[childViewsNb].$el.prependTo(this.$newsToday);
                }

                /** Sync DB **/
                this.childViews[childViewsNb].model.save(null, {
                    success: function(response) {
                        console.log('Successfully saved news');
                    },
                    error: function() {
                        console.log('Failed to save news');
                    }
                });
            },

            /** Destruction **/
            destroy: function () {

                _.each(this.childViews, function (view) {
                    view.destroy();
                }, this);

                /** Events **/
                this.undelegateEvents();
                this.$el.removeData().unbind();

                /** Remove from DOM **/
                this.remove();
                Backbone.View.prototype.remove.call(this);
            }
        });
    });