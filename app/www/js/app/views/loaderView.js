/**
 * Created by mickaelchanrion on 02/06/2016.
 */

define(
    ['jquery', 'underscore', 'backbone', 'tweenmax', 'templates/loaderTemplate'],
    function ($, _, Backbone, TweenMax, LoaderTemplate) {

        return Backbone.View.extend({

            el: "#loader",
            template: LoaderTemplate,

            initialize: function () {

                this.render();
            },

            render: function () {

                var storage = window.localStorage,
                    data = {
                        user: {
                            name: storage.getItem("userName"),
                            score: storage.getItem("userScore"),
                            avatar: storage.getItem("userAvatar")
                        },
                        swapUser: {
                            name: storage.getItem("swapUserName"),
                            score: storage.getItem("swapUserScore"),
                            avatar: storage.getItem("swapUserAvatar"),
                            theme: storage.getItem("swapUserTheme")
                        }
                    };

                this.$el.html(this.template(data));

                this.createShowAnimation();

                return this;
            },

            createShowAnimation: function () {

                this.showAnimation = new TimelineMax({
                    onCompleteScope: this,
                    onComplete: function () {
                        this.ready();
                    }
                });


                CustomEase.create("bounce", [
                    {s:0,cp:0.716,e:0.98},
                    {s:0.98,cp:1.244,e:1.056},
                    {s:1.056,cp:0.868,e:0.996},
                    {s:0.996,cp:1.124,e:1.012},
                    {s:1.012,cp:0.9,e:0.956},
                    {s:0.956,cp:1.012,e:1}]
                );

                this.showAnimation.to(this.$el.find("#loader__top, #loader__bottom"),.5, {autoAlpha: 1})
                    .from(this.$el.find("#swap-avatar"),1.2, {
                        rotation: "-=180",
                        autoAlpha: 0,
                        ease: CustomEase.byName("bounce"),
                        force3D: true
                    }, 2)
                    .from(this.$el.find("#no-swap-avatar"),1, {
                        rotation: "-=180",
                        autoAlpha: 0,
                        ease: CustomEase.byName("bounce"),
                        force3D: true
                    }, 2.1)
                    .from(this.$el.find("#swap-infos"),.5, {
                        x: "-100%",
                        ease: Back.easeOut.config(1.7),
                        force3D: true
                    }, 2.3)
                    .from(this.$el.find("#no-swap-infos"),.5, {
                        x: "100%",
                        ease: Back.easeOut.config(1.7),
                        force3D: true
                    }, 2.4)
                    .pause();


                _.defer(this.readyToShow.bind(this));
            },

            /** Show / Hide **/
            show: function (cb) {

                TweenMax.to(this.el, .5, {
                    autoAlpha: 1,
                    visibility: "visible",
                    ease: Quart.easeOut,
                    onCompleteScope: this,
                    onComplete: function () {
                        this.trigger("loaderView:visible")
                        cb && cb();
                    }
                });
            },

            hide: function () {

                TweenMax.to(this.$el, .5, {
                    autoAlpha: 0,
                    visibility: "hidden",
                    ease: Quart.easeOut,
                    callbackScope: this,
                    onComplete: function () {
                        this.trigger("loaderView:hidden");
                    }
                });
            },

            /** Ready **/
            readyToShow: function () {

                this.showAnimation.play(0);
            },

            ready: function () {

                this.isReady = true;
                this.trigger("loaderView:ready");
            },

            /** Destruction **/
            destroy: function () {

                /** Events **/
                this.undelegateEvents();
                this.$el.removeData().unbind();

                /** Remove DOM **/
                this.remove();
                Backbone.View.prototype.remove.call(this);
            }
        });
    }
);