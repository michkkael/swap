/**
 * Created by mickaelchanrion on 24/05/2016.
 */

define(
    ['jquery', 'underscore', 'backbone', 'models/newModel', 'templates/newTemplate'],
    function ($, _, Backbone, NewModel, NewTemplate) {

        return Backbone.View.extend({

            model: new NewModel(),
            template: NewTemplate,
            tagName: 'div',
            className: 'news__item new',

            events: {},

            initialize: function () {

                this.render();
            },

            render: function () {

                if (this.model.attributes.photo) this.$el.addClass("has-photo");

                this.$el.html(this.template(this.model.attributes));

                return this;
            },

            destroy: function () {

                /** Events **/
                this.undelegateEvents();
                this.$el.removeData().unbind();

                /** Remove DOM **/
                this.remove();
                Backbone.View.prototype.remove.call(this);
            }
        });
    }
);