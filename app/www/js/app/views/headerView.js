/**
 * Created by mickaelchanrion on 19/05/2016.
 */

define(
    ['jquery', 'underscore', 'backbone', 'tweenmax', 'draggable'],
    function ($, _, Backbone, TweenMax, Draggable) {

        return Backbone.View.extend({

            /** Events **/
            events: {
                'tap a': 'tap',
                'tap .main-header__hamburger': 'openMenu'
            },

            initialize: function () {

                this.$title = this.$el.find("#header-title");

                this.menu = {};
                this.menu.$el = this.$el.find('#main-menu');

                if (this.menu.$el.length) {
                    this.initMenu();
                }

                this.setUserAvatar(window.localStorage.getItem('swapUserAvatar'));

                var EventBus = require('events/eventBus');
                EventBus.on("Header:setTitle", this.setTitle, this);
            },

            setUserAvatar: function (id) {

                this.$el.find("#header-avatar").html('<img class="icon" src="images/png/avatar-header-' + id + '.png">');
            },

            setTitle: function (title) {

                this.$title.html(title);
            },

            initMenu: function () {

                this.menu.draggable = Draggable.create(this.menu.$el[0], {
                    trigger: "#main-menu, #main-header",
                    type: "y",
                    edgeResistance: 1,
                    zIndexBoost: false,
                    bounds: {
                        top: -window.innerHeight,
                        left: 0,
                        width: window.innerWidth,
                        height: 2 * window.innerHeight
                    },
                    throwProps: true,
                    force3D: true,
                    dragResistance: -0.5,
                    snap: {
                        y: function (endValue) {
                            return Math.round(endValue / window.innerHeight) * window.innerHeight;
                        }
                    },
                    onDragEnd: function () {
                        if (this.endY > window.innerHeight / 2) {
                            TweenMax.to(this.target, 1, {
                                y: window.innerHeight,
                                ease: Bounce.easeOut,
                                force3D: true
                            });
                        }
                    }
                });
            },

            openMenu: function () {
                TweenMax.to(this.menu.$el, 1, {
                    y: window.innerHeight,
                    ease: Bounce.easeOut,
                    force3D: true
                });
            },

            closeMenu: function () {
                TweenMax.set(this.menu.$el, {y: -window.innerHeight, delay: .5});
            },

            tap: function (e) {

                e.preventDefault();
                e.stopPropagation();

                var hash = $(e.currentTarget).attr('href');

                if (hash != Backbone.history.location.hash) {
                    this.closeMenu();
                    Backbone.history.navigate(hash, {trigger: true});
                }
            }
        });
    });
