/**
 * Created by mickaelchanrion on 02/06/2016.
 */

define(
    ['jquery', 'underscore', 'backbone', 'tweenmax', 'utils', 'events/eventBus', 'models/dialogModel', 'templates/dialog/defaultTemplate'],
    function ($, _, Backbone, TweenMax, Utils, EventBus, DialogModel, DefaultTemplate) {

        return Backbone.View.extend({

            el: "#dialog",
            template: DefaultTemplate,
            model: new DialogModel(),
            buffer: [],

            events: {

                /** Behavior **/
                'tap .do-cancel': 'cancel',
                'tap .do-showNextContent': 'showNextContent',

                /** Camera **/
                'tap .do-pickPhoto': 'openCamera',

                /** Feedback **/
                'tap .do-sendFeedback': 'sendFeedback',
                'tap .do-cancelSendFeedback': 'cancelSendFeedback',

                /** Request **/
                'tap .do-sendRequest': 'sendRequest',
                'tap .do-confirmRequest': 'confirmRequest',
                'tap .do-refuseRequest': 'refuseRequest',

                /** News **/
                'tap .do-sendNews': 'sendNews',

                /** Promise **/
                'tap .do-commentPromise': 'commentPromise'
            },

            initialize: function () {

                this.isBusy = false;

                /**
                 * To show a dialog: use this
                 * var EventBus = require('events/eventBus');
                 * EventBus.trigger("Dialog:open", {args});
                 **/
                EventBus.on("Dialog:open", this.open, this);
            },

            render: function (cb) {

                this.$el.html(this.template(this.model.attributes));

                cb && cb();

                _.defer(this.show.bind(this));

                return this;
            },

            open: function (data) {

                /** If dialog is already open, then wait to show new dialog **/
                if (this.isBusy) {
                    this.writeBuffer(data);
                    return;
                }

                this.isBusy = true;

                if (data.template) {
                    switch (data.template) {
                        case "addNews":

                            require(['templates/dialog/addNewsTemplate'],
                                (function (AddNewsTemplate) {
                                    this.template = AddNewsTemplate;
                                    this.model.set(data);
                                    this.render();
                                }).bind(this)
                            );
                            break;

                        case "alarm":

                            require(['templates/dialog/alarmTemplate'],
                                (function (AlarmTemplate) {
                                    this.template = AlarmTemplate;
                                    this.model.set(data);
                                    this.render();
                                }).bind(this)
                            );
                            break;

                        case "answerRequest":

                            require(['templates/dialog/answerRequestTemplate'],
                                (function (answerRequestTemplate) {
                                    this.template = answerRequestTemplate;
                                    this.model.set(data);
                                    this.render();
                                }).bind(this)
                            );
                            break;

                        case "feedback":

                            require(['templates/dialog/feedbackTemplate'],
                                (function (FeedbackTemplate) {
                                    this.template = FeedbackTemplate;
                                    this.model.set(data);
                                    this.render();
                                }).bind(this)
                            );
                            break;

                        case "promiseWon":

                            require(['templates/dialog/promiseWonTemplate'],
                                (function (PromiseWonTemplate) {
                                    this.template = PromiseWonTemplate;
                                    this.model.set(data);
                                    this.render();
                                }).bind(this)
                            );
                            break;

                        case "sendRequest":

                            require(['templates/dialog/sendRequestTemplate'],
                                (function (sendRequestTemplate) {
                                    this.template = sendRequestTemplate;
                                    this.model.set(data);
                                    this.render();
                                }).bind(this)
                            );
                            break;

                        case "default":
                        default:
                            this.template = DefaultTemplate;
                            this.model.set(data);
                            this.render();
                    }

                } else {
                    this.template = DefaultTemplate;
                    this.model.set(data);
                    this.render();
                }

            },

            cancel: function () {
                this.hide();
            },

            /** Buffer manager **/
            writeBuffer: function (data) {
                this.buffer.push(data);
            },

            drainBuffer: function () {
                this.buffer.splice(0, 1);
            },

            /** Camera **/
            openCamera: function (e) {

                this.photo = {};
                this.photo.el = e.currentTarget;

                EventBus.trigger("Camera:open", {
                    successCallback: this.savePhoto.bind(this)
                });
            },

            savePhoto: function (base64) {

                this.photo.data = base64;
                this.photo.el.className += " preview";
                this.photo.el.innerHTML = '<div id="photo-preview" style="background-image: url(data:image/jpeg;base64,' + base64 + ');"></div>';
            },

            /*encodeImageUri: function (imageUri) {

                var c = document.createElement('canvas'),
                    img = new Image();

                img.onload = function () {
                    c.width = this.width;
                    c.height = this.height;
                    c.getContext("2d").drawImage(img, 0, 0);
                };

                img.src = imageUri;

                /!** Return dataURL **!/
                return c.toDataURL("image/jpeg");
            },*/

            /** Animations **/
            hide: function (cb) {

                TweenMax.to(this.$el, .5, {
                    autoAlpha: 0,
                    visibility: "hidden",
                    ease: Quart.easeOut,
                    callbackScope: this,
                    onComplete: function () {

                        this.isBusy = false;

                        this.photo = null;

                        this.$el.html('');

                        cb && cb();

                        if (this.buffer.length) {
                            var data = this.buffer[0];
                            this.drainBuffer();
                            this.open(data)
                        }
                    }
                });
            },

            show: function () {

                TweenMax.to(this.el, .5, {
                    autoAlpha: 1,
                    visibility: "visible",
                    ease: Quart.easeOut
                });
            },

            showNextContent: function (e) {

                var wrapper = this.el.querySelector(".dialog__wrapper");
                var intro = this.$el.find("#intro")[0];
                var next = this.$el.find("#nextContent")[0];
                var tl = new TimelineMax();

                next.style.width = wrapper.clientWidth;
                next.style.height = wrapper.clientHeight;


                tl.to(intro, .5, {
                        y: -window.innerHeight,
                        ease: Back.easeIn.config(1.7),
                        force3D: true
                    })
                    .fromTo(next, .5, {
                        y: window.innerHeight,
                        visibility: "hidden",
                        ease: Quart.easeOut,
                        force3D: true
                    }, {
                        visibility: "visible",
                        y: 0
                    },.3);
            },

            /** Feedback **/
            sendFeedback: function () {

                /** If action is already triggered **/
                if (this.$el.find('.do-sendFeedback').hasClass('button--disabled')) return;

                this.$el.find('.do-sendFeedback').addClass('button--disabled');

                /** If it's a todo **/
                if (this.model.attributes.todo) {

                    var title = window.localStorage.getItem("userName") + " vient de " + this.model.attributes.todo.title.toLowerCase();

                    var profile = {
                        points: this.model.attributes.todo.points
                    };

                    var news = {
                        type: this.model.attributes.todo.type,
                        icon: this.model.attributes.todo.icon,
                        title: title,
                        user: {
                            slug: window.localStorage.getItem("userSlug"),
                            name: window.localStorage.getItem("userName"),
                            theme: window.localStorage.getItem("userTheme"),
                            avatar: window.localStorage.getItem("userAvatar"),
                            humor: this.$el.find('input:radio[id^="humor"]:checked')[0].value
                        },
                        comment: this.$el.find('#comment')[0].value,
                        date: new Date().toISOString()
                    };

                    if (this.photo) news.photo = this.photo.data;

                    this.hide(function () {

                        EventBus.trigger("Todo:achieved");
                        EventBus.trigger("Profile:update", profile);
                        EventBus.trigger("News:add", news);
                    });
                }
            },

            cancelSendFeedback: function () {

                /** If it's a todo **/
                if (this.model.attributes.todo) {
                    EventBus.trigger("Todo:notAchieved");
                }

                this.hide();
            },

            /** News **/
            sendNews: function () {

                var news = {
                    type: this.model.get("type") || "news",
                    icon: this.model.get("icon") || null,
                    user: {
                        slug: window.localStorage.getItem("userSlug"),
                        name: window.localStorage.getItem("userName"),
                        theme: window.localStorage.getItem("userTheme"),
                        avatar: window.localStorage.getItem("userAvatar"),
                        humor: this.$el.find('input:radio[id^="humor"]:checked')[0].value
                    },
                    title: window.localStorage.getItem("userName") + " :",
                    comment: this.$el.find('#comment')[0].value,
                    date: new Date().toISOString()
                };

                if (this.photo) news.photo = this.photo.data;

                /** Check if it's a report **/
                if (this.$el.find('input:checkbox[id="report"]:checked').length) {
                    news.type = "report";
                    news.title = window.localStorage.getItem("userName") + " a rapporté :";
                }

                /** Check if it's a promiseWon **/
                if (news.type == "promiseWon") {
                    news.title = news.user.name + " a débloqué la promesse faite par " + this.model.get("promise").by.name + "!";
                }

                this.hide(function () {
                    EventBus.trigger("News:add", news);
                });
            },

            /** Comment a promise won **/
            commentPromise: function () {

                /** If action is already triggered, then do nothing **/
                if (this.$el.find('.do-commentPromise').hasClass('button--disabled')) return;

                this.$el.find('.do-commentPromise').addClass('button--disabled');

                this.hide();

                this.open({
                    template: "addNews",
                    title: "Commente ta promesse !",
                    icon: "gift",
                    type: "promiseWon",
                    promise: this.model.get("promise")
                });
            },

            /** Request **/
            sendRequest: function () {

                /** If action is already triggered, then do nothing **/
                if (this.$el.find('.do-sendRequest').hasClass('button--disabled')) return;

                this.$el.find('.do-sendRequest').addClass('button--disabled');

                var data = {};
                data.message = this.model.attributes.sender.name +
                    " a besoin de ton aide pour " + this.model.attributes.todo.title.toLowerCase() + ".";

                data.recipient = {
                    slug: this.model.attributes.recipient.slug,
                    name: this.model.attributes.recipient.name
                };

                data.params = {
                    subject: this.model.attributes.subject,
                    sender: {
                        slug: this.model.attributes.sender.slug,
                        name: this.model.attributes.sender.name
                    },
                    todo: this.model.attributes.todo
                };

                Utils.post(
                    "http://swap-16.herokuapp.com/api/notify",
                    data,
                    this.hide.bind(this),
                    (function () {
                        this.$el.html('error');
                    }).bind(this)
                );
            },

            confirmRequest: function () {

                /** If action is already triggered, then do nothing **/
                if (this.$el.find('.do-sendRequest').hasClass('button--disabled')) return;

                this.$el.find('.do-sendRequest').addClass('button--disabled');

                this.answerRequest(this.model.attributes.sender.name + " a accepté votre demande !");
            },

            refuseRequest: function () {

                /** If action is already triggered, then do nothing **/
                if (this.$el.find('.do-sendRequest').hasClass('button--disabled')) return;

                this.$el.find('.do-sendRequest').addClass('button--disabled');

                this.answerRequest(this.model.attributes.sender.name + " a refusé votre demande.");
            },

            answerRequest: function (message) {

                var data = {};
                data.message = message;

                data.recipient = {
                    slug: this.model.attributes.recipient.slug,
                    name: this.model.attributes.recipient.name
                };

                data.params = {
                    subject: this.model.attributes.subject,
                    sender: {
                        slug: this.model.attributes.sender.slug,
                        name: this.model.attributes.sender.name
                    },
                    todo: this.model.attributes.todo
                };

                Utils.post(
                    "http://swap-16.herokuapp.com/api/notify",
                    data,
                    this.hide.bind(this),
                    (function () {
                        this.$el.html('error');
                    }).bind(this)
                );
            },

            /** Destruction **/
            destroy: function () {

                /** Events **/
                this.undelegateEvents();
                this.$el.removeData().unbind();

                /** Remove DOM **/
                this.remove();
                Backbone.View.prototype.remove.call(this);
            }
        });
    }
);