/**
 * Created by mickaelchanrion on 02/06/2016.
 */

define(
    ['jquery', 'underscore', 'backbone', 'tweenmax', 'utils', 'events/eventBus', 'templates/profileTemplate', 'models/profileModel'],
    function ($, _, Backbone, TweenMax, Utils, EventBus, ProfileTemplate, ProfileModel) {

        return Backbone.View.extend({

            icon: 'profile',
            title: 'Profil',
            model: new ProfileModel,
            template: ProfileTemplate,
            tagName: 'div',
            className: 'profile',

            initialize: function (params) {

                if (params) {
                    this.isChildView = _.has(params, "isChildView") ? params.isChildView : false;

                    if (this.isChildView) this.order = params.order || null;
                }

                /** Ready **/
                this.isReady = false;

                this.model.fetch().done(this.fetchDone.bind(this));

                /** Listen update profile request **/
                EventBus.on("Profile:update", this.update, this);
            },

            fetchDone: function () {

                var storage = window.localStorage;

                this.model.set("slug", storage.getItem("userSlug"));
                this.model.set("name", storage.getItem("userName"));
                this.model.set("theme", storage.getItem("userTheme"));
                this.model.set("score", parseInt(storage.getItem("userScore")));

                /** Demo: **/
                /*if (this.model.get("name") != "maxime") {
                    this.model.set("promise", null);
                }*/

                this.render();
            },

            /** Show / Hide **/
            show: function () {
                TweenMax.to(this.$el, 0.8, {css: {opacity: 1}, ease: "easeOutQuint"});
            },

            hide: function () {

                TweenMax.to(this.$el, 0.8, {
                    css: {opacity: 0},
                    ease: "easeInQuint",
                    callbackScope: this,
                    onComplete: function () {
                        if (!this.isChildView)
                            this.trigger("view:hidden");
                        else
                            this.trigger("childView:hidden");
                    }
                });
            },

            /** Ready **/
            ready: function () {

                this.isReady = true;

                if (!this.isChildView)
                    this.trigger("view:ready");
                else
                    this.trigger("childView:ready");
            },

            added: function () {
            },

            render: function () {

                if (this.model.length == 0) {
                    this.$el.html('<div class="news__wrapper">' + Utils.getHeadingTitle("Pas de données pour le profil :(") + '</div>');
                    this.ready();
                    return this;
                }

                /** Calc lastGoal and goal **/
                var score = this.model.get("score");

                if (score < 1000) {
                    this.model.set("lastGoal", Math.max([Math.trunc(score /= 100) * 100 - 100], [0]));
                    this.model.set("goal", this.model.get("lastGoal") + 200);
                } else {
                    this.model.set("lastGoal", Math.trunc(score /= 1000) * 1000);
                    this.model.set("goal", this.model.get("lastGoal") + 1000);
                }

                this.$el.html(this.template(this.model.attributes));

                this.prepareAnimation();

                _.defer(this.ready.bind(this));

                return this;
            },

            update: function (profile) {

                if (profile.points) {

                    /** Update score **/
                    var newScore = this.model.get("score") + parseInt(profile.points);
                    this.model.set("score", newScore);

                    /** Update successes **/
                    var successes = this.model.get("successes");
                    var successesChanged = false;
                    _.each(successes, function (success) {

                        if (success.type == "score" && !success.achieved && newScore >= success.minScore) {

                            success.achieved = true;
                            successesChanged = true;

                            _.delay((function () {
                                this.notifySuccessWon(success);
                            }).bind(this), 1000);

                        }
                    }, this);

                    if (successesChanged) this.model.set("successes", successes);

                    /** Check if goal reached **/
                    var promise = this.model.get("promise");
                    if (promise && newScore >= promise.goal) {

                        _.delay((function () {
                            this.notifyPromiseWon(promise);
                        }).bind(this), 1000);

                        this.model.set("promise", null);
                    }
                }

                this.render();
            },

            notifySuccessWon: function (success) {
                EventBus.trigger("Dialog:open", {
                    template: "default",
                    title: "Félicitations !",
                    message: "Tu as remporté le succès <br>\"" + success.label + "\"."
                });
            },

            notifyPromiseWon: function (promise) {
                EventBus.trigger("Dialog:open", {
                    template: "promiseWon",
                    title: "Bravo " + this.model.get("name") + "!",
                    score: this.model.get("score"),
                    subMessage: "Tu as atteint " + promise.goal + " points.",
                    message: "Tu viens de débloquer la promesse faite par " + promise.by.name + ".",
                    promise: promise
                });
            },

            prepareAnimation: function () {

                this.score = {
                    value: 0,
                    $el: this.$el.find("#user-score")
                };

                this.progressValue = (100 * (this.model.attributes.score - this.model.attributes.lastGoal)) / (this.model.attributes.goal - this.model.attributes.lastGoal);

                this.$gift = this.$el.find("#promise-gift");

                this.scoreAnimation = new TimelineMax();

                this.scoreAnimation.fromTo(this.score, 1, {
                    value: 0
                }, {
                    value: this.model.attributes.score,
                    roundProps: "value",
                    ease: Quart.easeOut,
                    force3D: true,
                    delay: .5,
                    callbackScope: this,
                    onUpdate: function () {
                        this.score.$el.html(this.score.value);
                    }
                }, 0);

                this.scoreAnimation.fromTo(this.$el.find("#progress-bar"), 1, {
                    width: 0
                }, {
                    width: this.progressValue + "%",
                    delay: .5,
                    ease: Quart.easeOut,
                    force3D: true
                }, 0);

                if (this.$gift.length) {
                    this.scoreAnimation.set(this.$gift, {
                        scale: 0,
                        rotation: 30
                    }, 0);

                    this.scoreAnimation.to(this.$gift, .3, {
                        scale: 1,
                        rotation: 0,
                        transformOrigin: "center",
                        delay: .5,
                        ease: Back.easeOut.config(3),
                        force3D: true
                    }, .2);
                }

                this.scoreAnimation.pause();
            },

            animateScore: function () {

                this.score.$el.html(0);

                this.scoreAnimation.play(0);
            },

            /** Destruction **/
            destroy: function () {

                /** Events **/
                this.undelegateEvents();
                this.$el.removeData().unbind();

                /** Remove from DOM **/
                this.remove();
                Backbone.View.prototype.remove.call(this);
            }
        });
    }
);