/**
 * Created by mickaelchanrion on 24/05/2016.
 */

define(
    ['jquery', 'underscore', 'backbone', 'tweenmax', 'events/eventBus', 'models/todoModel', 'templates/todoTemplate', 'utils'],
    function ($, _, Backbone, TweenMax, EventBus, TodoModel, TodoTemplate, Utils) {

        return Backbone.View.extend({

            model: new TodoModel(),
            template: TodoTemplate,
            tagName: 'div',
            className: 'todos__item todo',

            events: {
                'tap .do-toggleActions': 'toogleActions',
                'tap .do-exchange': 'exchange',
                'tap .do-help': 'help',
                'tap .do-validate': 'validate',
                'tap .do-showAlarm': 'showAlarm'
            },

            initialize: function () {

                this.menu = {};
                this.render();

                this.createMenuAnimation();
                this.createCompleteAnimation();
            },

            render: function () {

                this.$el.html(this.template(this.model.attributes));

                Utils.highlightText(this.$el.find('.todo__title'));

                this.menu.$el = this.$el.find(".todo__actions");
                this.menu.$buttons = this.menu.$el.find(".todo__button");
                TweenMax.set(this.menu.$el, {y: "-50%", scaleX: 0, transformOrigin: "left"});

                return this;
            },

            createMenuAnimation: function () {

                this.menuAnimation = new TimelineMax({
                    onComplete: function () {
                        this.menu.$el.addClass("open");
                    },
                    onCompleteScope: this,
                    onReverseComplete: function () {
                        this.menu.$el.removeClass("open");
                    },
                    onReverseCompleteScope: this
                });

                this.menuAnimation
                    .to(this.menu.$el, 0.4, {
                        scaleX: 1,
                        ease: Back.easeOut.config(1.7)
                    })
                    .staggerFrom(this.menu.$buttons, .2, {
                        x: "-30",
                        ease: Sine.easeOut,
                        force3D: true
                    }, .06, "-=0.4")
                    .pause();
            },

            createCompleteAnimation: function () {

                this.completeAnimation = new TimelineMax({});

                this.completeAnimation
                    .to(this.$el, .5, {
                        x: window.innerWidth,
                        ease: Back.easeIn.config(1.7),
                        delay: this.menuAnimation.totalTime(),
                        force3D: true
                    }).to(this.$el, .5, {
                    height: 0,
                    margin: 0,
                    ease: Quart.easeInOut,
                    callbackScope: this,
                    onComplete: this.destroy.bind(this)
                }).pause();
            },

            playCompleteAnimation: function () {

                this.completeAnimation.play(0);
            },

            toogleActions: function () {

                if (!this.menu.$el.hasClass("open")) {
                    this.menuAnimation.play(0)
                } else {
                    this.menuAnimation.reverse(0);
                }
            },

            exchange: function () {
                console.log("exchange");
            },

            help: function () {

                this.toogleActions();

                /** Open dialog to ask confirmation after menu is closed **/
                _.delay((function() {

                    EventBus.trigger("Dialog:open", {
                        template: "sendRequest",
                        subject: "request",
                        icon: this.model.get("icon"),
                        title: "Besoin d'aide ?",
                        message: "Tu as besoin d'aide pour <br> " + this.model.get("title").toLowerCase() + " ?",
                        recipient: {
                            slug: window.localStorage.getItem('swapUserSlug'),
                            name: window.localStorage.getItem('swapUserName')
                        },
                        sender: {
                            slug: window.localStorage.getItem('userSlug'),
                            name: window.localStorage.getItem('userName'),
                            avatar: window.localStorage.getItem("userAvatar")
                        },
                        todo: {
                            type: this.model.get("type"),
                            title: this.model.get("title"),
                            icon: this.model.get("icon")
                        }
                    });

                }).bind(this), this.menuAnimation.totalDuration()*1000);
            },

            validate: function () {

                this.toogleActions();

                /** Open dialog to ask for feedback after menu is closed **/
                _.delay((function() {

                    EventBus.trigger("Dialog:open", {
                        template: "feedback",
                        title: "Ton avis",
                        todo: {
                            type: this.model.get("type"),
                            icon: this.model.get("icon"),
                            title: this.model.get("title"),
                            points: this.model.get("points")
                        }
                    });

                }).bind(this), this.menuAnimation.totalDuration()*1000);

                /** When todo is achieved **/
                EventBus.once("Todo:achieved", this.achieved.bind(this));

                /** When todo is not achieved **/
                EventBus.once("Todo:notAchieved", (function () {

                    /** Remove event listener **/
                    EventBus.off("Todo:notAchieved");
                    EventBus.off("Todo:notAchieved");

                }).bind(this));
            },

            achieved: function () {

                /** Remove event listener **/
                EventBus.off("Todo:notAchieved");

                /** Hide todo and set achieved **/
                this.model.set("achieved", true);
                this.playCompleteAnimation();
            },

            showAlarm: function () {

                var swapUserName = window.localStorage.getItem("swapUserName");

                EventBus.trigger("Dialog:open", {
                    template: "alarm",
                    title: "Aujourd'hui, tu es " + swapUserName + " !",
                    message: "Et " + swapUserName + " a l'habitude de " + this.model.get("title").toLowerCase() + ".",
                    time: this.model.get("doBefore")
                });
            },

            destroy: function () {

                /** Events **/
                this.undelegateEvents();
                this.$el.removeData().unbind();

                /** Remove DOM **/
                this.remove();
                Backbone.View.prototype.remove.call(this);
            }
        });
    }
);