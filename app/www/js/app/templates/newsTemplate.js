/**
 * Created by mickaelchanrion on 24/05/2016.
 */

define(
    ['underscore'],
    function (_) {

        return _.template(
            '<div class="news__wrapper"></div>' +

            '<div class="news__floating-button button-add do-addNews"></div>'+

            '<div id="news__load-trigger" class="load-trigger">' +
                '<div class="button-reload"></div>' +
            '</div>'
        );
    }
);