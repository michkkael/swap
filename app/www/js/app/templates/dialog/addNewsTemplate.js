/**
 * Created by mickaelchanrion on 02/06/2016.
 */

define(
    ['underscore'],
    function (_) {

        return _.template(
            '<div class="dialog__wrapper template-addNews">' +
                '<div class="dialog__title"><%= title %></div>' +

                '<% var rates = 5; %>' +
                '<div id="rate" class="dialog__rate rate rate--contains-<%= rates %>">' +
                    '<% for (var i = 1; i <= rates; i++) { %>' +
                        '<input type="radio" name="humor" value="<%= i %>" id="humor-<%= i %>" <%= (i == 2) ? "checked" : "" %>>' +
                        '<label class="rate__item" for="humor-<%= i %>">' +
                            '<img class="icon" src="images/png/avatar-<%= window.localStorage.getItem(\"userAvatar\") %>-<%= i %>.png">' +
                        '</label>' +
                    '<% } %>' +
                    '<div class="rate__scale"></div>' +
                '</div>' +

                '<div class="dialog__section">' +

                    '<% try { if (type == "news") { %>' +
                        '<div class="dialog__checkbox checkbox">' +
                            '<input type="checkbox" id="report">' +
                            '<label class="checkbox__label" for="report">' +
                                '<span class="checkbox__icon"><svg class="icon"><use xlink:href="images/svg/sprite.symbol.svg#check"></use></svg></span>' +
                                'Je veux rapporter' +
                            '</label>' +
                        '</div>' +
                    '<% }} catch (e) { console.log("DialogView: ", e); } %>' +

                    '<div class="dialog__comment">' +
                        '<textarea id="comment" placeholder="Ton commentaire"></textarea><div class="close-keyboard"></div>' +
                        '<div class="dialog__photo do-pickPhoto">' +
                            '<svg><use xlink:href="images/svg/sprite.symbol.svg#camera"></use></svg>' +
                            '<span>Ajouter une photo</span>' +
                        '</div>' +
                    '</div>' +
                '</div>' +

                '<div class="dialog__actions">' +
                    '<div class="dialog__button button-cancel do-cancel"></div>' +
                    '<div class="dialog__button button-check do-sendNews"></div>' +
                '</div>' +
            '</div>'
        );
    }
);