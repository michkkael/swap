/**
 * Created by mickaelchanrion on 13/06/2016.
 */

define(
    ['underscore', 'utils'],
    function (_, Utils) {

        return _.template(
            '<div class="dialog__wrapper template-promise-won center">' +

                '<svg class="dialog__close do-cancel"><use xlink:href="images/svg/sprite.symbol.svg#close"></use></svg>' +

                '<div id="intro">' +
                    '<div class="dialog__title"><%= title %></div>' +

                    '<div class="dialog__score score">' +
                        '<svg class="score__icon"><use xlink:href="images/svg/sprite.symbol.svg#star"></use></svg>' +
                        '<div class="score__label"><%= score %></div>' +
                    '</div>' +

                    '<div class="dialog__message dialog__message--bold"><%= subMessage %></div>' +

                    '<div class="dialog__message"><%= message %></div>' +

                    '<div class="dialog__actions">' +
                        '<div class="dialog__button button-discover-promise do-showNextContent"></div>' +
                    '</div>' +
                '</div>' +

                '<div id="nextContent" class="promise">' +
                    '<div class="dialog__title promise__heading">' +

                        '<div class="promise__ray"><img class="icon" src="images/gif/ray.gif"></div>' +

                        '<% var colorTheme = Utils.getThemeColor("hex"); %>'+
                        '<?xml version="1.0" encoding="utf-8"?>' +
                        '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 356.7 245.5">' +
                            '<style type="text/css">.st0{fill:<% print(Utils.shadeColor(colorTheme, -.1)) %>;}.st1{fill:<% print(Utils.shadeColor(colorTheme, .3)) %>;}.st2{fill:#E5E5E5;}.st3{fill:#B3B3B3;}.st4{fill:#FFFFFF;}.st5{fill:none;}</style>' +
                            '<g id="explosion"><path class="st0" d="M297.4,162.4c-21.9,1.1-33.8,5.3-16.5,28.9c-28.4-7.1-27.5,5.3-19.7,25.5c-19.1-10.5-31.1-13-27.5,15.9c-20.9-20.3-26.1-9.2-29.3,12.3c-11.5-18.5-20.8-26.5-32.2,0.4c-8.4-28-18.9-21-33-4.2c-1.1-21.9-5.3-33.8-28.9-16.5c7.1-28.4-5.3-27.5-25.5-19.7c10.5-19.1,13-31.1-15.9-27.5c20.3-20.9,9.2-26.1-12.3-29.3c18.5-11.5,26.5-20.8-0.4-32.2c28-8.4,21-18.9,4.2-33c21.9-1.1,33.8-5.3,16.5-28.9c28.4,7.1,27.5-5.3,19.7-25.5c19.1,10.5,31.1,13,27.5-15.9c20.9,20.3,26.1,9.2,29.3-12.3c11.5,18.5,20.8,26.5,32.2-0.4c8.4,28,18.9,21,33,4.2c1.1,21.9,5.3,33.8,28.9,16.5c-7.1,28.4,5.3,27.5,25.5,19.7c-10.5,19.1-13,31.1,15.9,27.5c-20.3,20.9-9.2,26.1,12.3,29.3c-18.5,11.5-26.5,20.8,0.4,32.2C273.6,137.9,280.6,148.4,297.4,162.4z"/></g>' +
                            '<g id="gift"><path class="st1" d="M253.6,122.7C253.8,122.9,254,122.8,253.6,122.7L253.6,122.7z"/><path class="st1" d="M253.9,122c0-0.1,0-0.3,0-0.4v-17c0-14.6-9.9-27.9-24.5-27.9h-40.8l-0.1,46.4c33.2-0.1,64.1-0.4,64.6-0.4c0,0,0,0,0,0c0.2,0,0.6,0,0.7,0C253.8,122.7,253.9,122.4,253.9,122z"/><path class="st1" d="M173.2,139.3l-59.5-0.7c-0.7,0-2.9,0.1-3.4,0.6c-0.5,0.5-2.3,0.9-2.3,1.6v73.6c0,11.8,14.4,23.4,27.9,23.4H173L173.2,139.3z"/><polygon class="st1" points="113.4,138.1 113.6,138.1 113.6,138.1"/><path class="st1" d="M247.5,138.5l-58.6,0.9l-0.6,98.5h37.4c13.5,0,24.1-11.6,24.1-23.4v-73.3C249.9,139.7,249,138.5,247.5,138.5z"/><path class="st1" d="M220.1,34.9c-4.8-7.9-12.1-11.1-19.9-9c-7.4,2-14.4,8.7-18.8,17c-4.4-8.3-11.4-15-18.9-17c-7.9-2.1-15.1,1.5-20.1,9.7c-3.7,7.2-2.1,13.1-0.2,16.2c7.4,12,33.2,13.5,38.7,13.7c0,0,0-0.6,0.1-0.6c0,0,0,0,0,0c0.1,0,0.2,0,0.4,0c0.1,0,0.3,0,0.3,0c0,0,0,0,0,0c0,0,0,0.6,0.1,0.6c5.6-0.2,31.4-1.9,38.7-13.9C222.4,48.4,223.9,42.4,220.1,34.9z M149.6,46.1c-0.7-1.2-1.5-3.2,0.4-6.9c2.3-3.7,4.9-5.5,7.9-5.5c0.7,0,1.5,0.1,2.3,0.3c6.7,1.8,14.4,10.8,16.6,21C166.4,54.3,153,51.7,149.6,46.1z M213,46.1c-3.4,5.6-16.9,8.2-27.2,8.9c2.1-10.2,9.9-19.2,16.6-21c0.8-0.2,1.6-0.3,2.3-0.3c3,0,5.6,1.8,7.7,5.2C214.4,42.9,213.7,44.9,213,46.1z"/><path class="st1" d="M132.2,76.8c-14.5,0-28.3,13.3-28.3,27.9v16.9c0,1.4,2.1,1.6,3.5,1.6c0,0,31.8,0.1,65.6,0l0.2-46.4H132.2zM135.5,97c-12.2,2.5-10.8,14.7-10.8,15.2c0.3,2-1.1,3.8-3.1,4c-0.2,0-0.3,0-0.5,0c-1.8,0-3.3-1.3-3.6-3.1c-0.9-6.7,1.5-20.1,16.4-23.1c2-0.4,3.9,0.9,4.2,2.8C138.7,94.7,137.4,96.6,135.5,97z"/></g>' +
                            '<g id="banner"><path class="st2" d="M0,221c0,0,45,8.5,79.2,22.3l4.6-45c0,0-9.1-0.8-65.1-18.4l15.7,24.3L0,221z"/><path class="st2" d="M356.7,221c0,0-45,8.5-79.2,22.3l-4.6-45c0,0,9.1-0.8,65.1-18.4l-15.7,24.3L356.7,221z"/><path class="st3" d="M49.1,219.7c0,0,16.6,17.6,30.2,23.5l19.3-49.5l-30-18.3L49.1,219.7z"/><path class="st3" d="M307.8,219.7c0,0-16.6,17.6-30.2,23.5l-19.3-49.5l30-18.3L307.8,219.7z"/><path class="st4" d="M178.4,192.4c-41.8,0-80.1-6.4-109.9-16.9l-19.5,44.3c31.5,15.3,77.8,25,129.4,25c51.6,0,97.9-9.7,129.4-25l-19.5-44.3C258.5,186,220.2,192.4,178.4,192.4z"/><path id="SVGID_x5F_1_x5F_" class="st5" d="M55.4,208.4c0,0,138.2,47.3,246.2,0.3"/>' +
                                '<text><textPath  xlink:href="#SVGID_x5F_1_x5F_" startOffset="10.1813%">' +
                                    '<tspan  style="stroke:#000000;stroke-width:0.5;stroke-miterlimit:10; font-family:"All Things Pink Regular"; font-size:28px;"><%= promise.title %></tspan>' +
                                '</textPath></text>' +
                            '</g>' +
                        '</svg>' +
                    '</div>' +

                    '<div class="dialog__message promise__message">"<%= promise.text %>"</div>' +

                    '<div class="dialog__actions">' +
                        '<div class="dialog__button button-comment-promise do-commentPromise"></div>' +
                    '</div>' +
                '</div>' +
            '</div>'
        );
    }
);