/**
 * Created by mickaelchanrion on 02/06/2016.
 */

define(
    ['underscore'],
    function (_) {

        return _.template(
            '<div class="dialog__wrapper template-send-request center">' +
                '<div class="dialog__title"><%= title %></div>' +

                '<% try { %>' +
                    '<div class="dialog__icon featured-item featured-item--fill">' +
                        '<div class="featured-item__background"><svg class="icon"><use xlink:href="images/svg/sprite.symbol.svg#featured-background"></use></svg></div>' +
                        '<div class="featured-item__picto"><svg class="icon"><use xlink:href="images/svg/sprite.symbol.svg#<%= icon %>"></use></svg></div>' +
                    '</div>' +
                '<% } catch (e) { console.log("DialogView: ", e); } %>' +

                '<% try { %>' +
                    '<div class="dialog__message"><%= message %></div>' +
                '<% } catch (e) { console.log("DialogView: ", e); } %>' +

                '<div class="dialog__actions">' +
                    '<div class="dialog__button button-cancel do-cancel"></div>' +
                    '<div class="dialog__button button-check do-sendRequest"></div>' +
                '</div>' +
            '</div>'
        );
    }
);