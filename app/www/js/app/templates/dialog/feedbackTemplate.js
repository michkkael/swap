/**
 * Created by mickaelchanrion on 02/06/2016.
 */

define(
    ['underscore'],
    function (_) {

        return _.template(
            '<div class="dialog__wrapper template-feedback">' +
                '<div class="dialog__title"><%= title %></div>' +

                '<div class="dialog__question">' +
                    'As-tu aimé <br> <%= todo.title.toLowerCase() %> ?' +
                '</div>' +

                '<% var rates = 5; %>' +
                '<div id="rate" class="dialog__rate rate rate--contains-<%= rates %>">' +
                    '<% for (var i = 1; i <= rates; i++) { %>' +
                        '<input type="radio" name="humor" value="<%= i %>" id="humor-<%= i %>" <%= (i == 2) ? "checked" : "" %>>' +
                        '<label class="rate__item" for="humor-<%= i %>">' +
                            '<img class="icon" src="images/png/avatar-<%= window.localStorage.getItem(\"userAvatar\") %>-<%= i %>.png">' +
                        '</label>' +
                    '<% } %>' +
                    '<div class="rate__scale"></div>' +
                '</div>' +

                '<div class="dialog__comment">' +
                    '<textarea id="comment" placeholder="Ton commentaire"></textarea><div class="close-keyboard"></div>' +
                    '<div class="dialog__photo do-pickPhoto">' +
                        '<svg><use xlink:href="images/svg/sprite.symbol.svg#camera"></use></svg>' +
                        '<span>Ajouter une photo</span>' +
                    '</div>' +
                '</div>' +

                '<div class="dialog__actions">' +
                    '<div class="dialog__button button-cancel do-cancelSendFeedback"></div>' +
                    '<div class="dialog__button button-check do-sendFeedback"></div>' +
                '</div>' +
            '</div>'
        );
    }
);