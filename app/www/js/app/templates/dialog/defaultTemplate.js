/**
 * Created by mickaelchanrion on 02/06/2016.
 */

define(
    ['underscore'],
    function (_) {

        return _.template(
            '<div class="dialog__wrapper template-default center">' +
                '<div class="dialog__title"><%= title %></div>' +

                '<% try { %>' +
                    '<div class="dialog__message"><%= message %></div>' +
                '<% } catch(e) { console.log("DialogView: ", e); }%>' +

                '<div class="dialog__actions">' +
                    '<div class="dialog__button button-check do-cancel"></div>' +
                '</div>' +
            '</div>'
        );
    }
);