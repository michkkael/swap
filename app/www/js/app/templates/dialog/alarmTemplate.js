/**
 * Created by mickaelchanrion on 13/06/2016.
 */

define(
    ['underscore'],
    function (_) {

        return _.template(
            '<div class="dialog__wrapper template-alarm center">' +
                '<div class="dialog__title"><%= title %></div>' +

                '<div class="dialog__alarm alarm">' +
                    '<svg class="alarm__icon"><use xlink:href="images/svg/sprite.symbol.svg#clock--theme"></use></svg>' +
                    '<div class="alarm__label"><%= time %></div>' +
                '</div>' +

                '<div class="dialog__message"><%= message %></div>' +

                '<div class="dialog__actions">' +
                    '<div class="dialog__button button-check do-cancel"></div>' +
                '</div>' +
            '</div>'
        );
    }
);