/**
 * Created by mickaelchanrion on 02/06/2016.
 */

define(
    ['underscore'],
    function (_) {

        return _.template(
            '<div class="dialog__wrapper template-answer-request center">' +

                '<div class="dialog__title"><%= title %></div>' +

                '<% try { %>' +
                    '<div class="dialog__icon featured-item featured-item--fill" data-theme="<%= swapUser.theme %>">' +
                        '<div class="featured-item__background"><svg class="icon"><use xlink:href="images/svg/sprite.symbol.svg#featured-background"></use></svg></div>' +
                        '<div class="featured-item__picto--up"><svg class="icon"><use xlink:href="images/svg/sprite.symbol.svg#<%= todo.icon %>"></use></svg></div>' +
                        '<div class="featured-item__user"><img class="icon" src="images/png/avatar-featured-<%= swapUser.avatar %>-2.png"></div>' +
                    '</div>' +
                '<% } catch (e) { console.log("DialogView: ", e); } %>' +

                '<div class="dialog__message"><%= message %></div>' +

                '<div class="dialog__actions">' +
                    '<div class="dialog__button button-cancel do-refuseRequest"></div>' +
                    '<div class="dialog__button button-check do-confirmRequest"></div>' +
                '</div>' +
            '</div>'
        );
    }
);