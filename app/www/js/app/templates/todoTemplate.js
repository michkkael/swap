/**
 * Created by mickaelchanrion on 24/05/2016.
 */

define(
    ['underscore'],
    function (_) {

        return _.template(
            '<div class="todo__icon featured-item featured-item--stroke do-toggleActions">' +
                '<div class="featured-item__background"><svg class="icon"><use xlink:href="images/svg/sprite.symbol.svg#featured-background"></use></svg></div>' +
                '<div class="featured-item__picto"><svg class="icon"><use xlink:href="images/svg/sprite.symbol.svg#<%= icon %>"></use></svg></div>' +
            '</div>' +
            '<div class="todo__content">' +
                '<div class="todo__title"><%= title %></div>' +
                '<div class="todo__infos">' +
                    '<div class="todo__points">' +
                        '<svg class="icon"><use xlink:href="images/svg/sprite.symbol.svg#star"></use></svg>' +
                        '<span><%= points %> points</span>' +
                    '</div>' +
                    '<% if (doBefore){ %>' +
                        '<div class="todo__time do-showAlarm">' +
                            '<svg class="icon"><use xlink:href="images/svg/sprite.symbol.svg#clock"></use></svg>' +
                            '<span>À <%= doBefore %></span>' +
                        '</div>' +
                    '<% } %>' +
                '</div>' +
                '<div class="todo__actions">' +
                    '<div class="todo__button button-exchange button-exchange--disabled do-exchange"></div>' +
                    '<div class="todo__button button-help do-help"></div>' +
                    '<div class="todo__button button-check do-validate"></div>' +
                '</div>' +
            '</div>'
        );
    }
);