/**
 * Created by mickaelchanrion on 24/05/2016.
 */

define(
    ['underscore', 'utils'],
    function (_, Utils) {

        return _.template(
            '<div class="todos__wrapper">' +

                '<% if (task) { %>' +
                    Utils.getHeadingTitle("Dans la journée, tu vas devoir") +
                    '<div class="todos__list" id="tasks"></div>' +
                '<% } %>' +

                '<% if (hobby) { %>' +
                    Utils.getHeadingTitle("Pour te détendre, tu pourras") +
                    '<div class="todos__list" id="hobbies"></div>' +
                '<% } %>' +

                '<% if (!task && !hobby) { %>' +
                    Utils.getHeadingTitle("Tu n'as plus rien à faire pour aujourd'hui !") +
                '<% } %>' +

            '</div>'

            //'<div class="todos__floating-button button-add button-add--disabled do-addTodo"></div>'
        );
    }
);