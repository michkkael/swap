/**
 * Created by mickaelchanrion on 24/05/2016.
 */

define(
    ['underscore'],
    function (_) {

        return _.template(
            '<div class="new__icon featured-item featured-item--fill" data-theme="<%= user.theme %>">' +
                '<div class="featured-item__background"><svg class="icon"><use xlink:href="images/svg/sprite.symbol.svg#featured-background"></use></svg></div>' +
                '<div class="featured-item__picto--up"><svg class="icon"><use xlink:href="images/svg/sprite.symbol.svg#<%= icon %>"></use></svg></div>' +
                '<div class="featured-item__user"><img class="icon" src="images/png/avatar-featured-<%= user.avatar %>-<%= user.humor %>.png"></div>' +
            '</div>' +
            '<div class="new__content">' +
                '<div class="new__title"><%= title %></div>' +
                '<div class="new__infos">' +
                    '<% if (comment){ %>' +
                        '<div class="new__comment">"<%= comment %>"</div>' +
                    '<% } %>' +
                '</div>' +
            '</div>' +
            '<% if (photo){ %>' +
                //'<div class="new__photo" style="background-image: url(data:image/jpeg;base64,<%= photo %>)"></div>' +
                '<div class="new__photo"><img src="data:image/jpeg;base64,<%= photo %>"></div>' +
            '<% } %>'
        );
    }
);