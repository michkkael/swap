/**
 * Created by mickaelchanrion on 02/06/2016.
 */

define(
    ['underscore', 'utils'],
    function (_, Utils) {

        return _.template(
            '<div class="profile__wrapper">' +
                Utils.getHeadingTitle("Ton score") +
                '<div class="profile__infos infos">' +
                    '<div class="infos__score score">' +
                        '<svg class="score__icon"><use xlink:href="images/svg/sprite.symbol.svg#star"></use></svg>' +
                        '<div class="score__label" id="user-score">0</div>' +
                    '</div>' +

                    '<% if (promise) { %>' +
                        '<div class="infos__tip">Atteins <span class="roboto-bold"><%= promise.goal %> points</span> et découvre la promesse de <%= promise.by.name %> !</div>' +
                    '<% } %>' +

                    '<% var avatar = window.localStorage.getItem("userAvatar") %>' +
                    '<img class="infos__avatar" src="images/png/avatar-<%= avatar %>-1.png">' +

                    '<div class="infos__level level">' +
                        '<div class="level__last-goal"><%= lastGoal %><span>points</span></div>' +
                        '<div class="level__progress progress">' +
                            '<div class="progress__bar" id="progress-bar"></div>' +
                        '</div>'+
                        '<div class="level__goal"><%= goal %><span>points</span>' +
                            '<% if (promise) { %>' +
                                '<svg id="promise-gift"><use xlink:href="images/svg/sprite.symbol.svg#gift"></use></svg>' +
                            '<% } %>' +
                        '</div>' +
                    '</div>' +

                '</div>' +

                Utils.getHeadingTitle("Tes succès") +
                '<div class="profile__successes successes">' +
                    '<div class="successes__list">' +
                        '<% _.each(successes, function (success) { %>' +
                            '<div class="successes__item success <% if (success.achieved) print(\'success--achieved\') %>">' +
                                '<svg class="success__icon"><use xlink:href="images/svg/sprite.symbol.svg#trophy-<%= success.id %>"></use></svg>' +
                                '<div class="success__label"><%= success.label %></div>' +
                            '</div>' +
                        '<% }, this); %>' +
                    '</div>' +
                '</div>' +
            '</div>'
        );
    }
);