/**
 * Created by mickaelchanrion on 24/05/2016.
 */

define(
    ['underscore'],
    function (_) {

        return _.template(
            '<div id="loader__top" class="swap" data-theme="<%= swapUser.theme %>">' +

                '<div id="swap-infos" class="loader__infos">' +
                    '<div class="loader__score score">' +
                        '<svg class="score__icon"><use xlink:href="images/svg/sprite.symbol.svg#star"></use></svg>' +
                        '<div class="score__label"><%= swapUser.score %></div>' +
                    '</div>' +

                    '<div class="loader__user-name"><%= swapUser.name %></div>' +
                '</div>' +

                '<img id="swap-avatar" class="loader__user-avatar" src="images/png/avatar-loader-<%= swapUser.avatar %>.png">' +
            '</div>' +

            '<div id="loader__bottom" class="no-swap">' +

                '<div id="no-swap-infos" class="loader__infos">' +
                    '<div class="loader__user-name"><%= user.name %></div>' +

                    '<div class="loader__score score">' +
                        '<svg class="score__icon"><use xlink:href="images/svg/sprite.symbol.svg#star"></use></svg>' +
                        '<div class="score__label"><%= user.score %></div>' +
                    '</div>' +
                '</div>' +

                '<img id="no-swap-avatar" class="loader__user-avatar" src="images/png/avatar-loader-<%= user.avatar %>.png">' +
            '</div>'
        );
    }
);