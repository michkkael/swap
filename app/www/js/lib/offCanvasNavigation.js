/**
 * Created by mickaelchanrion on 26/05/2016.
 */

;(function ($, TweenMax, Draggable) {

    var defaults = {
        startSlide: 1,

        // Callbacks
        onReady: function () {},
        onSlideChange: function () {}
    };

    $.fn.offCanvasNavigation = function (options) {

        if (this.length == 0) return this;

        // support mutltiple elements
        if (this.length > 1) {
            this.each(function () {
                $(this).offCanvasNavigation();
            });
            return this;
        }

        // create a namespace to be used throughout the plugin
        var ocn = {};
        // set a reference to our slider element
        var el = this;

        /**
         * ===================================================================================
         * = PRIVATE FUNCTIONS
         * ===================================================================================
         */

        var init = function () {

            ocn.settings = $.extend({}, defaults, options);

            ocn.wrapper = el.find(".ocn__wrapper");
            ocn.slides = el.find(".ocn__slide");
            ocn.triggers = el.find(".ocn__trigger");

            if (ocn.triggers.length > 0) registerTriggers();

            makeDraggable();

            ocn.enabled = true;
            ocn.initialized = true;
        };

        var makeDraggable = function () {

            var nbSlides = ocn.slides.length,
                viewportWidth = window.innerWidth;

            ocn.wrapper.width(viewportWidth * nbSlides);
            ocn.currentIndex = 1;
            ocn.currentPosition = 0;

            setTimeout(function () {
                if (ocn.settings.startSlide != 1) moveTo(getTranslationValue(ocn.settings.startSlide), false);

                ocn.settings.onReady();
            }, 5);

            ocn.draggable = Draggable.create(ocn.wrapper, {
                type: "x",
                edgeResistance: .65,
                bounds: {
                    top: 0,
                    left: -viewportWidth * (nbSlides - 1),
                    width: viewportWidth * (2 * nbSlides - 1),
                    height: window.innerHeight
                },
                zIndexBoost: false,
                throwProps: true,
                force3D: true,
                snap: {
                    x: function (endValue) {
                        return Math.round(endValue / viewportWidth) * viewportWidth;
                    }
                },
                onDragEnd: function () {
                    var value = Math.round(this.endX / viewportWidth) * viewportWidth,
                        direction = this.getDirection(),
                        prevX = ocn.currentPosition,
                        maxTranslate;

                    /** Force slide by slide navigation **/

                    if (value == prevX)
                        return;

                    else if (direction == "left") {

                        maxTranslate = prevX - viewportWidth;

                        if (value > maxTranslate)
                            return; // value too low
                    }

                    else if (direction == "right") {

                        maxTranslate = prevX + viewportWidth;

                        if (value < maxTranslate)
                            return; // value too low
                    }

                    value = maxTranslate;

                    moveTo(value);
                },
                onThrowComplete: function () {
                    setCurrentIndex(this.x);
                }
            });
        };

        var registerTriggers = function () {
            ocn.triggers.on('click', onTapTrigger);
        };

        var unregisterTriggers = function () {
            ocn.triggers.off('click');
        };

        var onTapTrigger = function (e) {

            e.preventDefault();

            var value = getTranslationValue($(e.currentTarget).data("trigger"));

            if (value != null) moveTo(value);
        };

        var getCurrentPosition = function () {
            return ocn.currentPosition;
        };

        var setCurrentPosition = function (position) {
            ocn.currentPosition = position;
        };

        var getCurrentIndex = function () {
            return ocn.currentIndex;
        };

        var setCurrentIndex = function (position) {

            var oldIndex = ocn.currentIndex;

            setCurrentPosition(position);

            if (position == 0)
                ocn.currentIndex = 1;
            else
                ocn.currentIndex = Math.abs(position) / window.innerWidth + 1;

            // onSlideChange callback
            if (oldIndex != ocn.currentIndex)
                ocn.settings.onSlideChange(oldIndex, ocn.currentIndex);
        };

        var getTranslationValue = function (target) {

            if (ocn.currentIndex == target) return null;

            else if (ocn.currentIndex < target)
                return -1 * (target - ocn.currentIndex) * window.innerWidth + ocn.currentPosition;

            else
                return (ocn.currentIndex - target) * window.innerWidth + ocn.currentPosition;
        };

        var moveTo = function (value, animated) {

            animated = (typeof animated === 'undefined') ? true : animated;

            if (!ocn.enabled) return;

            if (animated) {

                TweenMax.to(ocn.wrapper, .5, {
                    x: value,
                    ease: Quart.easeOut,
                    force3D: true,
                    onComplete: setCurrentIndex(value)
                });

            } else {

                TweenMax.set(ocn.wrapper, {
                    x: value,
                    force3D: true,
                    onComplete: setCurrentIndex(value)
                });
            }
        };

        /**
         * ===================================================================================
         * = PUBLIC FUNCTIONS
         * ===================================================================================
         */

        el.getStatus = function () {
            if (ocn.enabled) {
                return "enabled";
            } else if (!ocn.enabled) {
                return "disabled";
            } else if (ocn.initialized) {
                return "initialized";
            } else {
                return "killed";
            }
        };

        el.goToSlide = function (slide, animated) {

            animated = (typeof animated === 'undefined') ? true : animated;

            var value = getTranslationValue(slide);

            if (value != null) moveTo(value, animated);
        };

        el.enable = function () {

            if (ocn.enabled) return;

            ocn.draggable[0].enable();
            if (ocn.triggers.length > 0) registerTriggers();

            ocn.enabled = true;
        };

        el.disable = function () {

            if (!ocn.enabled) return;

            ocn.draggable[0].disable();
            if (ocn.triggers.length > 0) unregisterTriggers();

            ocn.enabled = false;
        };

        el.kill = function () {

            ocn.draggable[0].kill();
            if (ocn.triggers.length > 0) unregisterTriggers();

            ocn.enabled = false;
            ocn.initialized = false;
        };

        el.reload = function () {

            el.kill();
            init();
        };

        init();

        // returns the current jQuery object
        return this;
    };

})(jQuery, TweenMax, Draggable);