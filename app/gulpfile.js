var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    svgSprite = require('gulp-svg-sprite'),
    browserSync = require('browser-sync').create();


gulp.task('default', ['compile-sass', 'watch', 'browser-sync'], function () {
});


gulp.task('watch', function () {
    gulp.watch('www/styles/**/*.scss', ['compile-sass']);
});


gulp.task('compile-sass', function () {
    gulp.src('www/styles/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('www/styles'))
        .pipe(browserSync.stream());
});

gulp.task('browser-sync', function () {
    browserSync.init({
        //proxy: "http://local.site.com/"
        server: {
            baseDir: "www/"
        }
    });

    gulp.watch("www/styles/**/*.scss", ['compile-sass']);
    gulp.watch("www/*.html").on('change', browserSync.reload);
    gulp.watch("www/js/**/*.js").on('change', browserSync.reload);
});

gulp.task('sprite-svg', function () {

    var configCss = {
        shape: {
            dimension: {
                maxWidth: 1000,
                maxHeight: 1000
            },
            spacing: {
                padding: 1,
                box: "padding"
            }
            //dest: 'images/svg/intermediate-svg' // Keep the intermediate files
        },
        mode: {
            css: {
                bust: false,
                render: {
                    scss: {
                        dest: 'partials/utils/_sprite.scss'
                    }
                },
                dest: '.',
                sprite: '../images/svg/sprite.css.svg',
                example: {
                    dest: '../images/svg/sprite.css.html'
                }
            }
        }
    };

    var configSymbol = {
        shape: {
            dimension: {
                maxWidth: 100,
                maxHeight: 100
            }
            //dest: 'images/svg/intermediate-svg' // Keep the intermediate files
        },
        mode: {
            symbol: {
                dest: '.',
                sprite: 'sprite.symbol.svg',
                example: {
                    dest: 'sprite.symbol.html'
                }
            }
        }
    };

    gulp.src('www/images/svg/raw/*.svg')
        .pipe(svgSprite(configCss))
        .pipe(gulp.dest('www/styles'));

    gulp.src('www/images/svg/raw/*.svg')
        .pipe(svgSprite(configSymbol))
        .pipe(gulp.dest('www/images/svg'));
});