/**
 * Created by mickaelchanrion on 24/05/2016.
 */

define(
    ['backbone', 'models/achievementModel'],
    function (Backbone, AchievementModel) {

        return Backbone.Collection.extend({

            url: 'api/achievements.json',
            model: AchievementModel
        });
    }
);