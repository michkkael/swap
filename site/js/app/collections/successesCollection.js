/**
 * Created by mickaelchanrion on 24/05/2016.
 */

define(
    ['backbone', 'models/successModel'],
    function (Backbone, successModel) {

        return Backbone.Collection.extend({

            url: 'api/successes.json',
            model: successModel
        });
    }
);