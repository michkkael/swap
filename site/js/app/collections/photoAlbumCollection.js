/**
 * Created by mickaelchanrion on 24/05/2016.
 */

define(
    ['backbone', 'models/photoModel'],
    function (Backbone, PhotoModel) {

        return Backbone.Collection.extend({

            url: 'http://swap-16.herokuapp.com/api/news',
            model: PhotoModel
        });
    }
);