/**
 * Created by mickaelchanrion on 03/06/2016.
 */

define(
    ['underscore', 'backbone'],
    function (_, Backbone) {

        var EventBus = {};

        _.extend(EventBus, Backbone.Events);

        return EventBus;
    }
);