/**
 * Created by mickaelchanrion on 19/05/2016.
 */

/** Config require **/
requirejs.config({
    baseUrl: 'js',
    paths: {
        jquery: '../node_modules/jquery/dist/jquery.min',
        underscore: '../node_modules/underscore/underscore-min',
        backbone: '../node_modules/backbone/backbone-min',
        tweenmax: '../node_modules/gsap/src/minified/TweenMax.min',
        moment: '../node_modules/moment/min/moment-with-locales.min',
        scrollmagic: '../node_modules/scrollmagic/scrollmagic/minified/ScrollMagic.min',

        router: './app/router/router',
        collections: './app/collections',
        models: './app/models',
        templates: './app/templates',
        views: './app/views',
        events: './app/events',

        roundProps: './lib/RoundProps',
        scrollTo: './lib/ScrollTo'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        tweenmax: {
            deps: ['roundProps'],
            exports: 'TweenMax'
        },
        scrollTo: {
            deps: ['tweenmax']
        },
        scrollmagic: {
            exports: 'ScrollMagic'
        }
    }
});

/** Starts require **/
requirejs(['jquery', 'app/views/appView', 'moment'], function($, App, Moment){

    Moment.locale('fr');

    /** Launch application **/
    new App();
});