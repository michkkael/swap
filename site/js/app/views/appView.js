/**
 * Created by mickaelchanrion on 19/05/2016.
 */

define(
    ['jquery', 'underscore', 'backbone', 'router', 'views/loaderView', 'views/headerView', 'views/homeView', 'views/progressView'],
    function ($, _, Backbone, Router, LoaderView, HeaderView, HomeView, ProgressView) {

        return Backbone.View.extend({

            /** Html **/
            el: 'body',

            /** Constructor **/
            initialize: function () {

                if (window.devicePixelRatio > 1) {
                    this.el.classList += " retina";
                }

                if (!this.loaderView) {

                    /** Initialize loader **/
                    this.loaderView = new LoaderView();
                    this.loaderHidden = false;

                    if (!this.loaderView.isReady) {

                        /** Waiting loaderView, when it's done, continue initialization **/
                        this.listenToOnce(this.loaderView, "loaderView:ready", this.initialize);

                        return;
                    }
                }

                this.currentView = null;

                this.$container = this.$("#main");

                this.header = new HeaderView({el: '#main-header'});

                this.router = new Router();
                this.listenTo(this.router, 'home', this.goHome);
                this.listenTo(this.router, 'progress', this.goProgress);

                this.router.start();
            },

            /** View Management **/
            killView: function () {

                /** Remove current view **/
                if (this.currentView) {

                    this.stopListening(this.currentView);
                    this.currentView.destroy();
                }
            },

            addView: function () {

                /** Add new view **/
                this.$container.prepend(this.currentView.el);

                //this.currentView.added();

                /** If currentView is not ready, then listenTo, else hide loader **/
                if (!this.currentView.isReady) {
                    this.listenToOnce(this.currentView, 'view:ready', this.viewReady);
                } else {
                    _.defer(this.viewReady.bind(this))
                }
            },

            viewReady: function () {
                this.hideLoader();
            },

            /** Show / hide loader **/
            hideLoader: function () {

                /** Page ready, hide loader **/
                this.loaderHidden = true;
                this.loaderView.hide();
            },

            showLoader: function (method, params) {

                /** Show loader and return to previous method when it's done **/
                this.listenToOnce(this.loaderView, "loaderView:visible", method.bind(this, params));
                this.loaderView.show();
                this.loaderHidden = false;
            },

            /** Routes **/
            goHome: function () {

                this.header.select();

                /** If loader is hidden, then show it, else kill current view **/
                if (this.loaderHidden) {
                    this.showLoader(this.goProgress());
                    return;
                } else {
                    this.killView();
                }

                /** Update current view **/
                this.currentView = new HomeView();

                /** Add view **/
                this.addView();
            },

            goProgress: function () {

                /** Selectionne la navigation **/
                this.header.select();

                /** If loader is hidden, then show it, else kill current view **/
                if (this.loaderHidden) {
                    this.showLoader(this.goProgress);
                    return;
                } else {
                    this.killView();
                }

                /** Update current view **/
                this.currentView = new ProgressView();

                /** Add view **/
                this.addView();
            }
        });
    });
