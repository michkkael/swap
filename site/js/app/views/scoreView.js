/**
 * Created by mickaelchanrion on 17/06/2016.
 */

define(
    ['jquery', 'underscore', 'backbone', 'tweenmax', 'scrollTo', 'events/eventBus', 'lib/text!../templates/scoreTemplate.html', 'models/scoreModel'],
    function ($, _, Backbone, TweenMax, ScrollTo, EventBus, ScoreTemplate, ScoreModel) {

        return Backbone.View.extend({

            name: "scoreView",
            model: new ScoreModel,
            template: _.template(ScoreTemplate),
            tagName: 'section',
            id: 'score',

            initialize: function (params) {

                if (params) {
                    this.isChildView = _.has(params, "isChildView") ? params.isChildView : false;
                }

                /** Ready **/
                this.isReady = false;

                this.model.fetch().done(this.fetchDone.bind(this));
            },

            fetchDone: function () {

                this.listenTo(this.model, "change", this.update);

                this.render();
            },

            /** Ready **/
            ready: function () {

                this.isReady = true;

                if (!this.isChildView)
                    this.trigger("view:ready");
                else
                    this.trigger("childView:ready");

                this.createProgressAnimation();
                this.createAvatarAnimation();

                EventBus.on("scoreView:setMaximumScore", this.setMaximumScore.bind(this));
            },

            added: function () {
            },

            render: function () {

                this.$el.html(this.template(this.model.attributes));

                _.defer(this.ready.bind(this));

                return this;
            },

            createProgressAnimation: function () {

                this.progressBar = {};
                this.progressBar.el = $('#score__progress-bar').get(0);
                this.progressBar.barLength = this.progressBar.el.getTotalLength();

                this.progressBar.el.style.strokeDasharray = this.progressBar.barLength + ' ' + this.progressBar.barLength;
                this.progressBar.el.style.strokeDashoffset = this.progressBar.barLength;

                this.score = {
                    value: 0,
                    $el: this.$el.find("#score-counter__label")
                };

                EventBus.on("scoreView:playProgressAnimation", this.progressAnimation.bind(this));
            },

            progressAnimation: function (score) {

                var value = score || this.model.get("score");

                var tl = new TimelineMax();
                tl.to(this.progressBar.el, 1, {
                    strokeDashoffset: this.getProgressValue(value),
                    ease: Quart.easeOut,
                    onCompleteScope: this,
                    onComplete: function () {
                        if (score == this.model.get("promise").goal) {
                            this.promiseWon();
                        }
                    }
                });

                tl.fromTo(this.score, 1, {
                    value: 0
                }, {
                    value: value,
                    roundProps: "value",
                    ease: Quart.easeOut,
                    force3D: true,
                    callbackScope: this,
                    onUpdate: function () {
                        this.score.$el.html(this.score.value);
                    }
                }, 0);
            },

            getProgressValue: function (score) {
                return (100 - (100 * score) / this.model.get("promise").goal) / 100 * this.progressBar.barLength;
            },

            createAvatarAnimation: function () {

                this.avatarAnimation = new TimelineMax();

                this.avatarAnimation
                    .from(this.$el.find(".avatar-1"), 0.7, {
                        y: "200vh",
                        rotation: 20,
                        ease: Back.easeOut.config(1.5),
                        force3D: true
                    }, 0.2)
                    .from(this.$el.find(".avatar-2"), 0.7, {
                        y: "200vh",
                        rotation: -180,
                        ease: Back.easeOut.config(1.5),
                        force3D: true
                    }, 0.15)
                    .from(this.$el.find(".avatar-3"), 0.7, {
                        y: "200vh",
                        rotation: 80,
                        ease: Back.easeOut.config(1.5),
                        force3D: true
                    }, 0)
                    .from(this.$el.find(".avatar-4"), 0.7, {
                        y: "200vh",
                        rotation: 90,
                        ease: Back.easeOut.config(1.5),
                        force3D: true
                    }, 0.1)
                    .pause();

                EventBus.on("scoreView:playAvatarsAnimation", (function () {
                    this.avatarAnimation.play(0);
                }).bind(this));
            },

            update: function () {

                this.progressAnimation(this.model.get("score"));
            },

            setMaximumScore: function () {

                TweenMax.to(window, 1, {
                    scrollTo: {
                        y: $("#score").get(0).scrollTop
                    },
                    ease: Power2.easeInOut,
                    onCompleteScope: this,
                    onComplete: function () {
                        this.model.set("score", this.model.get("promise").goal);
                    }
                });
            },

            promiseWon: function () {

                TweenMax.to($("#promise-screen"), 1, {
                    autoAlpha: 1,
                    ease: Quart.easeOut
                })
            },

            /** Destruction **/
            destroy: function () {

                /** Events **/
                this.undelegateEvents();
                this.$el.removeData().unbind();

                /** Remove from DOM **/
                this.remove();
                Backbone.View.prototype.remove.call(this);
            }
        });
    }
);