/**
 * Created by mickaelchanrion on 16/06/2016.
 */

define([
    'jquery', 'underscore', 'backbone','moment', 'models/photoModel', 'lib/text!../templates/photoTemplate.html'
], function ($, _, Backbone, Moment, PhotoModel, PhotoTemplate) {

    return Backbone.View.extend({

        name: 'photoView',
        model: new PhotoModel,
        template: _.template(PhotoTemplate),
        tagName: 'article',
        className: 'photo-album__item photo',

        initialize: function () {

            this.render();
        },

        render: function () {

            this.model.set("date", Moment(this.model.get("date")).format('dddd [à] H[h]mm'));

            this.$el.html(this.template(this.model.attributes));

            return this;
        },

        destroy: function () {

            /** Events **/
            this.undelegateEvents();
            this.$el.removeData().unbind();

            /** Remove DOM **/
            this.remove();
            Backbone.View.prototype.remove.call(this);
        }
    })

});