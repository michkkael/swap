/**
 * Created by mickaelchanrion on 19/05/2016.
 */

define(
    ['jquery', 'underscore', 'backbone', 'tweenmax', 'lib/text!../templates/homeTemplate.html'],
    function ($, _, Backbone, TweenMax, HomeTemplate) {

        var HomeView = Backbone.View.extend({

            /** Html **/
            tagName: 'section',
            className: 'home',
            template: _.template(HomeTemplate),

            initialize: function () {

                /** Ready **/
                this.isReady = false;

                this.childViews = {
                    //successesView: new SuccessesView()
                };

                /** Listen all childViews **/
                /*_.each(this.childViews, function (childView) {
                    this.listenToOnce(childView, "childView:ready", this.childViewReady.bind(this, childView));
                }, this);*/

                /** Model + render **/
                //this.listenTo(this.model, 'sync', this.render);
                //this.model.fetch();

                this.render();
            },

            /** Show / Hide **/
            show: function () {
                TweenMax.to(this.el, 0.8, {autoAlpha: 1, ease: Quart.easeOut});
            },

            hide: function () {
                TweenMax.to(this.el, 0.8, {
                    autoAlpha: 0,
                    ease: Quart.easeIn,
                    onComplete: this.hidden.bind(this)
                });
            },

            hidden: function () {
                this.trigger('hidden');
            },

            /** Ready **/
            ready: function () {
                this.isReady = true;
                this.trigger('view:ready');
            },

            childViewReady: function (childView) {

                /** Add view to ocn slide **/
                childView.$el.appendTo();

                /** Waiting for all views ready **/
                if (_.findWhere(this.childViews, {isReady: false})) return;
                else this.render();
            },

            added: function () {
            },

            /** Render **/
            render: function () {

                /** Html **/
                this.$el.html(this.template());

                this.ready();
            },

            /** Destruction **/
            destroy: function () {

                _.each(this.childViews, function (view) {
                    view.destroy();
                }, this);

                /** Events **/
                this.undelegateEvents();
                this.$el.removeData().unbind();

                /** Remove DOM **/
                this.remove();
                Backbone.View.prototype.remove.call(this);
            }
        });

        return HomeView;
    });
