/**
 * Created by mickaelchanrion on 24/05/2016.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'tweenmax',
    'events/eventBus',
    'collections/achievementsCollection',
    'lib/text!../templates/achievementsTemplate.html',
    'views/achievementView'
], function ($, _, Backbone, TweenMax, EventBus, AchievementsCollection, AchievementsTemplate, AchievementView) {

    return Backbone.View.extend({

        name: "achievementsView",
        collection: new AchievementsCollection(),
        template: _.template(AchievementsTemplate),
        tagName: 'section',
        id: 'achievements',

        events: {},

        initialize: function (params) {

            if (params) {
                this.isChildView = _.has(params, "isChildView") ? params.isChildView : false;
            }

            /** Ready **/
            this.isReady = false;

            this.childViews = {};

            this.collection.fetch().done(this.fetchDone.bind(this));
        },

        fetchDone: function () {

            this.render();
        },

        /** Ready **/
        ready: function () {

            this.isReady = true;

            if (!this.isChildView)
                this.trigger("view:ready");
            else
                this.trigger("childView:ready");
        },

        added: function () {
        },

        /** Render **/
        render: function () {

            /** If there's news **/
            if (this.collection.models.length == 0) {
                this.$el.html("Pas de tâches et loisirs réalisées...");
                _.defer(this.ready.bind(this));
                return this;
            }

            /** Set template and get wrapper **/
            this.$el.html(this.template());
            this.$wrapper = this.$el.find(".wrapper");

            /** Add childView to wrapper **/
            var i = 0;
            _.each(this.collection.models, function (model) {

                this.childViews[i] = new AchievementView({model: model});
                this.childViews[i].$el.appendTo(this.$wrapper);

                i++;

            }, this);

            _.defer(this.ready.bind(this));

            return this;
        },

        /** Destruction **/
        destroy: function () {

            _.each(this.childViews, function (view) {
                view.destroy();
            }, this);

            /** Events **/
            this.undelegateEvents();
            this.$el.removeData().unbind();

            /** Remove from DOM **/
            this.remove();
            Backbone.View.prototype.remove.call(this);
        }
    });
});