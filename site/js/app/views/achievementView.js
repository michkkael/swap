/**
 * Created by mickaelchanrion on 16/06/2016.
 */

define([
    'jquery', 'underscore', 'backbone', 'tweenmax', 'events/eventBus', 'moment', 'models/achievementModel', 'lib/text!../templates/achievementTemplate.html'
], function ($, _, Backbone, Tweenmax, EventBus, Moment, AchievementModel, AchievementTemplate) {

    return Backbone.View.extend({

        name: 'achievementView',
        model: new AchievementModel,
        template: _.template(AchievementTemplate),
        tagName: 'article',
        className: 'achievements__item achievement',

        events: {
            'click .do-getPoints': 'getPoints'
        },

        initialize: function () {

            this.render();
        },

        render: function () {

            this.$el.html(this.template(this.model.attributes));

            this.createProgressAnimation();

            return this;
        },

        createProgressAnimation: function () {

            var selector = "#graph__progress--"+this.model.get("user").slug;


            this.progressGraph = {};
            this.progressGraph.el = this.$el.find(selector).get(0);
            this.progressGraph.length = this.$el.find(selector).get(0).getTotalLength();

            this.progressGraph.el.style.strokeDasharray = this.progressGraph.length + ' ' + this.progressGraph.length;
            this.progressGraph.el.style.strokeDashoffset = this.progressGraph.length;

            this.percentage = {
                value: 0,
                $el: this.$el.find(".graph-progress span")
            };

            EventBus.on("achievementView:playProgressAnimation", this.progressAnimation.bind(this));
        },

        progressAnimation: function () {

            var tl = new TimelineMax();

            tl.fromTo(this.progressGraph.el, 1, {
                strokeDashoffset: this.progressGraph.length
            }, {
                strokeDashoffset: this.getProgressValue(this.model.get("user").progress),
                ease: Quart.easeInOut
            });

            tl.fromTo(this.percentage, 1, {
                value: 0
            }, {
                value: this.model.get("user").progress,
                roundProps: "value",
                ease: Quart.easeOut,
                force3D: true,
                callbackScope: this,
                onUpdate: function () {
                    this.percentage.$el.html(this.percentage.value);
                }
            }, 0);
        },

        getProgressValue: function (val) {
            return (100 - (100 * val) / 100) / 100 * this.progressGraph.length;
        },

        getPoints: function (e) {

            e.preventDefault();

            this.bonus = {};
            this.bonus.$el = this.$el.find(".score-counter__label span");
            this.bonus.value = parseInt(this.bonus.$el.html());

            console.log(this.bonus.$el);

            console.log(parseInt(this.bonus.$el.html()))

            Tweenmax.fromTo(this.bonus, 1, {
                value: parseInt(this.bonus.$el.html())
            }, {
                value: 0,
                roundProps: "value",
                ease: Quart.easeInOut,
                callbackScope: this,
                onUpdate: function () {
                    console.log(this.bonus.value);
                    this.bonus.$el.html(this.bonus.value);
                },
                onComplete: function () {
                    EventBus.trigger("scoreView:setMaximumScore");
                }
            });
        },

        destroy: function () {

            /** Events **/
            this.undelegateEvents();
            this.$el.removeData().unbind();

            /** Remove DOM **/
            this.remove();
            Backbone.View.prototype.remove.call(this);
        }
    });
});