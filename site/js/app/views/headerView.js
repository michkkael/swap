/**
 * Created by mickaelchanrion on 19/05/2016.
 */

define(
    ['jquery', 'underscore', 'backbone', 'tweenmax'],
    function ($, _, Backbone, TweenMax) {

        return Backbone.View.extend({

            /** Events **/
            events: {
                'click a': 'click'
            },

            initialize: function () {

                /** Nav **/
                this.$navList = this.$el.find('#main-header__menu ul');
            },

            /** Select **/
            select: function () {

                var $links = this.$navList.find("li a");

                $links.removeClass("active");
                $links.filter('[href="#' + Backbone.history.fragment + '"]').addClass("active");
            },

            click: function (e) {

                e.preventDefault();
                e.stopPropagation();
                Backbone.history.navigate($(e.currentTarget).attr('href'), {trigger: true});

            }
        });
    }
);
