/**
 * Created by mickaelchanrion on 16/06/2016.
 */

define([
    'jquery', 'underscore', 'backbone','moment', 'models/successModel', 'lib/text!../templates/successTemplate.html'
], function ($, _, Backbone, Moment, SuccessModel, SuccessTemplate) {

    return Backbone.View.extend({

        name: 'successView',
        model: new SuccessModel,
        template: _.template(SuccessTemplate),
        tagName: 'article',
        className: 'successes__item success',

        initialize: function () {

            this.render();
        },

        render: function () {

            this.$el.html(this.template(this.model.attributes));

            return this;
        },

        destroy: function () {

            /** Events **/
            this.undelegateEvents();
            this.$el.removeData().unbind();

            /** Remove DOM **/
            this.remove();
            Backbone.View.prototype.remove.call(this);
        }
    });
});