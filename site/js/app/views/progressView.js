/**
 * Created by mickaelchanrion on 19/05/2016.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'tweenmax',
    'scrollmagic',
    'events/eventBus',
    'lib/text!../templates/progressTemplate.html',
    'views/scoreView',
    'views/successesView',
    'views/achievementsView',
    'views/photoAlbumView'
], function ($, _, Backbone, TweenMax, ScrollMagic, EventBus, ProgressTemplate, ScoreView, SuccessesView, AchievementsView, PhotoAlbumView) {

    return Backbone.View.extend({

        name: 'progressView',
        tagName: 'section',
        id: 'progress',
        template: _.template(ProgressTemplate),

        initialize: function () {

            /** Ready **/
            this.isReady = false;

            this.childViews = {
                scoreView: new ScoreView({isChildView: true}),
                successesView: new SuccessesView({isChildView: true}),
                achievementsView: new AchievementsView({isChildView: true}),
                photoAlbumView: new PhotoAlbumView({isChildView: true})
            };

            /** Listen all childViews **/
            _.each(this.childViews, function (childView) {
                this.listenToOnce(childView, "childView:ready", this.render.bind(this, childView));
            }, this);

            this.$el.html(this.template());
        },

        /** Show / Hide **/
        show: function () {
            TweenMax.to(this.el, 0.8, {autoAlpha: 1, ease: Quart.easeOut});
        },

        hide: function () {
            TweenMax.to(this.el, 0.8, {
                autoAlpha: 0,
                ease: Quart.easeIn,
                onComplete: this.hidden.bind(this)
            });
        },

        hidden: function () {
            this.trigger('hidden');
        },

        /** Ready **/
        ready: function () {
            this.isReady = true;
            this.trigger('view:ready');

            _.delay(function () {
                EventBus.trigger("scoreView:playProgressAnimation");
                EventBus.trigger("scoreView:playAvatarsAnimation");
            }, 1000);


            var controller = new ScrollMagic.Controller();

            new ScrollMagic.Scene({triggerElement: "#achievements"})
                .on("enter", function () {
                    EventBus.trigger("achievementView:playProgressAnimation");
                })
                .on("leave", function () {})
                .addTo(controller);
        },

        added: function () {
        },

        /** Render **/
        render: function (childView) {

            /** Add childView **/
            switch (childView.name) {

                case "scoreView":
                    childView.$el.appendTo(this.$el.find("#progress__score"));
                    break;

                case "successesView":
                    childView.$el.appendTo(this.$el.find("#progress__successes"));
                    break;

                case "achievementsView":
                    childView.$el.appendTo(this.$el.find("#progress__achievements"));
                    break;

                case "photoAlbumView":
                    childView.$el.appendTo(this.$el.find("#progress__photo-album"));
            }

            /** Waiting for all views ready **/
            if (_.findWhere(this.childViews, {isReady: false})) return;
            else this.ready();
        },

        /** Destruction **/
        destroy: function () {

            _.each(this.childViews, function (view) {
                view.destroy();
            }, this);

            /** Events **/
            this.undelegateEvents();
            this.$el.removeData().unbind();

            /** Remove DOM **/
            this.remove();
            Backbone.View.prototype.remove.call(this);
        }
    });
});
