/**
 * Created by mickaelchanrion on 16/06/2016.
 */

define(
    ['jquery', 'underscore', 'backbone', 'tweenmax'],
    function ($, _, Backbone, Tweenmax) {


        return Backbone.View.extend({

            el: "#loader",

            initialize: function () {

                this.render();
            },

            render: function () {

                _.defer(this.ready.bind(this));

                return this;
            },

            /** Show / Hide **/
            show: function (cb) {

                var tlShow = new TimelineMax({
                    onCompleteScope: this,
                    onComplete: function () {

                        this.trigger("loaderView:visible");
                        cb && cb();
                    }
                });

                tlShow.fromTo(this.el, 0.3, {
                    y: -window.innerHeight,
                    ease: Quart.easeOut
                }, {
                    y: 0
                });
            },

            hide: function (cb) {

                Tweenmax.to(this.el, 0.3, {
                    y: window.innerHeight,
                    ease: Quart.easeIn,
                    callbackScope: this,
                    onComplete: function () {

                        this.trigger("loaderView:hidden");
                        cb && cb();
                    }
                });
            },

            ready: function () {

                this.isReady = true;
                this.trigger("loaderView:ready");
            },

            /** Destruction **/
            destroy: function () {

                /** Events **/
                this.undelegateEvents();
                this.$el.removeData().unbind();

                /** Remove DOM **/
                this.remove();
                Backbone.View.prototype.remove.call(this);
            }
        });

    }
);