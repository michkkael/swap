/**
 * Created by mickaelchanrion on 17/06/2016.
 */

define(
    ['backbone'],
    function (Backbone) {

        return Backbone.Model.extend({

            url: 'api/score.json',
            defaults: {
                score: null,
                promise: null
            }
        });
    }
);