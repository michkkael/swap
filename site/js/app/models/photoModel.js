/**
 * Created by mickaelchanrion on 24/05/2016.
 */

define(
    ['backbone'],
    function (Backbone) {

        return Backbone.Model.extend({

            defaults: {
                type: 'news',
                icon: null,
                title: 'title',
                user: {
                    slug: 'user-slug',
                    name: 'user-name',
                    theme: 1,
                    avatar: 0,
                    humor: 0
                },
                comment: null,
                photo: null,
                date: new Date().toISOString()
            }
        });
    }
);