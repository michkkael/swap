/**
 * Created by mickaelchanrion on 19/05/2016.
 */

define(['jquery', 'underscore', 'backbone'], function($, _, Backbone){

    return Backbone.Router.extend({

        /** Routes **/
        routes: {
            '': 'home',
            'progress': 'progress'
        },

        /** Start **/
        start: function () {
            /** Start listening URLs **/
            Backbone.history.start();
        },

        /** Routes **/
        home: function () {
            /** Pass args : this.trigger('event', args) **/
            this.trigger('home');
        },

        progress: function () {
            this.trigger('progress');
        }

    });
});
