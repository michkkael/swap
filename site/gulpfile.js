var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    svgSprite = require('gulp-svg-sprite'),
    browserSync = require('browser-sync').create();



gulp.task('default', ['compile-sass', 'watch', 'browser-sync'], function(){});


gulp.task('watch', function(){
    gulp.watch('./styles/**/*.scss', ['compile-sass']);
});


gulp.task('compile-sass', function(){
    gulp.src('./styles/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./styles'))
        .pipe(browserSync.stream());
});

gulp.task('browser-sync', function(){
    browserSync.init({
        server: {
            baseDir: "."
        }
    });

    gulp.watch("./styles/**/*.scss", ['compile-sass']);
    gulp.watch("./*.html").on('change', browserSync.reload);
    gulp.watch("./js/**/*.js").on('change', browserSync.reload);
});

gulp.task('sprite-svg', function () {

    var configCss = {
        shape: {
            dimension: {
                maxWidth: 1000,
                maxHeight: 1000
            },
            spacing: {
                padding: 1,
                box: "padding"
            }
            //dest: 'images/svg/intermediate-svg'
        },
        mode: {
            css: {
                bust: false,
                render: {
                    scss: {
                        dest: 'partials/utils/_sprite.scss'
                    }
                },
                dest: '.',
                sprite: '../images/svg/sprite.css.svg',
                example: {
                    dest: '../images/svg/sprite.css.html'
                }
            }
        }
    };

    var configSymbol = {
        shape: {
            dimension: {
                maxWidth: 100,
                maxHeight: 100
            }
        },
        mode: {
            symbol: {
                dest: '.',
                sprite: 'sprite.symbol.svg',
                example: {
                    dest: 'sprite.symbol.html'
                }
            }
        }
    };

    gulp.src('images/svg/raw/*.svg')
        .pipe(svgSprite(configCss))
        .pipe(gulp.dest('styles'));

    gulp.src('images/svg/raw/*.svg')
        .pipe(svgSprite(configSymbol))
        .pipe(gulp.dest('images/svg'));
});